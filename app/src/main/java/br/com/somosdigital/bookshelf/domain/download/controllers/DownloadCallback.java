package br.com.somosdigital.bookshelf.domain.download.controllers;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;

/**
 * Created by marcus on 29/09/16.
 */

public interface DownloadCallback {

    void nextBook();

    void downloadActivity();

    void activityError(Activity activity);

    void bookError(Book book, String distribuidorErrorCode);

    void oedError(Oed oed, String distribuidorErrorCode);

    void unzipActivity(Activity activity);

    void unzipOed(Oed oed);

    void unzipBook(Book book);
}
