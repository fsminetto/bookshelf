package br.com.somosdigital.bookshelf.presentation.bookshelf.oed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.File;

import br.com.solucaoadapta.shared.BuildConfig;
import br.com.solucaoadapta.shared.storage.runtime.ClassStatusManager;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.utils.state.BookshelfLibraryState;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation.ReaderOEDCallback;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderObject;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.utils.OedManagerAndroid;

/**
 * Created by marcus on 28/10/16.
 */

public class OedFragment extends Fragment implements OedContract.View {

    private WebView readerView;

    private OedPresenter presenter;

    private ReaderOEDCallback readerOEDCallback;

    private ReaderObject objectCallback;

    private OedCommunication oedCommunication;

    private final static String oedPlayerUrl =
            "file:///android_asset/PlayerOED/playerOED.html?pathOed=";

    private String objectPath;

    private String bookId;

    private OedInfo oedInfo;

    public static OedFragment newInstance(String objectPath, String bookID, OedInfo oedInfo) {
        OedFragment oedFragment = new OedFragment();

        Bundle bundle = new Bundle();
        bundle.putString("objectPath", objectPath);
        bundle.putString("bookId", bookID);
        bundle.putParcelable("oedInfo", oedInfo);

        oedFragment.setArguments(bundle);
        return oedFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.oed_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebView.setWebContentsDebuggingEnabled(BuildConfig.WEBVIEW_DEBUG);

        getInfo(getArguments());

        readerView = view.findViewById(R.id.oed_view);
        readerView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        readerView.getSettings().setJavaScriptEnabled(true);

        presenter = new OedPresenter();
        presenter.attachView(this);

        objectCallback = presenter.getObjectCallback();

        readerOEDCallback = new ReaderOEDCallback(getContext(), objectCallback);
        readerView.addJavascriptInterface(readerOEDCallback, "AndroidInterface");

        if (oedInfo != null) {
            presenter.loadOed(objectPath, oedInfo.getId(), bookId, new OedManagerAndroid(getContext(), BookshelfLibraryState.getPreferenceName()));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            oedCommunication = (OedCommunication) getActivity();
        }
    }

    @Override
    public void openMedia(String mediaPath) {
        String userId = ClassStatusManager.getInstance().getCurrentUser().getId();
        int index = mediaPath.lastIndexOf("/");
        String mediaFilename = mediaPath.substring(index+1, mediaPath.length());
        mediaFilename = userId + "_" + oedInfo.getId() + "_" + mediaFilename;

        File newMedia = new File(getContext().getFilesDir().getPath() + "/" + mediaFilename);

        if(newMedia.exists()) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromFile(newMedia));
            if(newMedia.getName().contains("mp4")) {
                intent.setDataAndType(Uri.fromFile(newMedia), "video/mp4");
            } else if(newMedia.getName().contains("mp3")) {
                intent.setDataAndType(Uri.fromFile(newMedia), "audio/mp3");
            }

            startActivity(intent);
        }
    }


    @Override
    public void onOedReadyLocally(String objectPath, String key){
        readerView.loadUrl(oedPlayerUrl + objectPath + "/index.html&oedKey=" + key);
    }

    @Override
    public void loadOedOnline(){
        readerView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        readerView.loadUrl(oedInfo.getLinkPreview());
    }

    public void getInfo(Bundle bundle){
        if (bundle != null) {
            objectPath = bundle.getString("objectPath");
            bookId = bundle.getString("bookId");
            oedInfo = bundle.getParcelable("oedInfo");
        }
    }

    @Override
    public void showServerMaintenanceError() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (presenter != null) {
            presenter.detachView();
        }

        if (readerView != null) {
            readerView.destroy();
        }
    }
}

