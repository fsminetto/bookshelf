package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation;


import android.content.Context;
import android.webkit.JavascriptInterface;

import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderOED;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderObject;

/**
 * Created by luis.afonso on 11/07/2015.
 */
public class ReaderOEDCallback implements ReaderOED {
    private final Context context;
    private final ReaderObject Object;

    public ReaderOEDCallback(Context context, ReaderObject Object) {
        this.context = context;
        this.Object = Object;
    }


    @JavascriptInterface
    @Override
    public boolean playMedia(String oedMediaPath) {
            Object.onOpenMedia(oedMediaPath);
            return true;
    }
}