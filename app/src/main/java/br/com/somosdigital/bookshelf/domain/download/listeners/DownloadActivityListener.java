package br.com.somosdigital.bookshelf.domain.download.listeners;

import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadActivityListener implements DownloadStatusListener {
    private Book book;

    private Activity activity;

    private DownloadCallback downloadCallback;

    private Log log;

    private int checkProgress = 0;

    private long lastDownloadedBytes = 0;

    public DownloadActivityListener(Book book, Activity activity, DownloadCallback downloadCallback, Log log) {
        this.book = book;
        this.activity = activity;
        this.downloadCallback = downloadCallback;
        this.log = log;
    }

    @Override
    public void onDownloadComplete(int id) {
        downloadCallback.unzipActivity(activity);
        log.logInfo("Download Activity Complete");
    }

    @Override
    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
        downloadCallback.activityError(activity);
    }

    @Override
    public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {
        if (checkProgress + 2 == progress) {

            book.setCurrentBytesDownloaded(book.getCurrentBytesDownloaded()
                    + (downloadedBytes - lastDownloadedBytes));

            if(book.getCardProgress() != null) {
                book.getCardProgress().onProgressUpdate((DownloadInformation
                        .getProgress(book)));
            }

            checkProgress = progress;
            lastDownloadedBytes = downloadedBytes;
        }
    }

    @Override
    public void onPause(int id, long downloadedBytes) {
        log.logInfo("Download Activity Pausing");

        DownloadInformation.setCurrentBookId("");
        activity.setDownloadStatus(DownloadStatus.PAUSED);
        book.setGlobalDownloadStatus(DownloadStatus.PAUSED);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        downloadCallback.nextBook();

        log.logInfo("Download Activity Paused");
    }

    @Override
    public void onDownloadCanceled(int id) {
        log.logInfo("Download Activity canceling");

        DownloadInformation.setCurrentBookId("");
        book.setGlobalDownloadStatus(DownloadStatus.CANCELING);
        activity.setDownloadStatus(DownloadStatus.CANCELING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        removeBookFiles();

        book.setDownloadStatus(DownloadStatus.NEW);
        book.setGlobalDownloadStatus(DownloadStatus.NEW);
        activity.setDownloadStatus(DownloadStatus.NEW);
        book.setCurrentBytesDownloaded(0);

        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        downloadCallback.nextBook();

        log.logInfo("Download Activity Canceled");
    }

    private void removeBookFiles() {
        FileManager.deleteFile(book.getCompactedPath());
        FileManager.removeDirectory(book.getTempPath());
        FileManager.deleteFile(book.getBrkPath());
        FileManager.removeDirectory(book.getBookPath());
    }
}
