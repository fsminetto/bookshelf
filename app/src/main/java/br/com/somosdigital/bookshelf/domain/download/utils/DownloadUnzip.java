package br.com.somosdigital.bookshelf.domain.download.utils;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import io.reactivex.Observable;

/**
 * Created by marcus on 17/04/17.
 */

public interface DownloadUnzip {

    Observable<Boolean> unzipBook(Book book);

    Observable<Boolean> unzipActivity(Activity activity);

    Observable<Boolean> unzipOed(Oed oed);

    Observable<Boolean> unzipTemplate(Template template);

}
