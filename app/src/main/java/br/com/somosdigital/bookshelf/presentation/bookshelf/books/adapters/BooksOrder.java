package br.com.somosdigital.bookshelf.presentation.bookshelf.books.adapters;

/**
 * Created by marcus on 19/10/16.
 */

public class BooksOrder {

    public static final int TITLE = 0;

    public static final int AUTHOR = 1;

    public static final int SUBJECT = 2;

    public static final int YEAR = 3;

    public static final int COLLECTION = 4;

}
