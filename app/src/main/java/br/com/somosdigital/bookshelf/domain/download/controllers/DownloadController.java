package br.com.somosdigital.bookshelf.domain.download.controllers;

import java.io.File;

import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BooksValues;
import br.com.somosdigital.bookshelf.domain.download.listeners.DownloadActivityListener;
import br.com.somosdigital.bookshelf.domain.download.listeners.DownloadBookListener;
import br.com.somosdigital.bookshelf.domain.download.listeners.DownloadOedListener;
import br.com.somosdigital.bookshelf.domain.download.triggers.DownloadActivity;
import br.com.somosdigital.bookshelf.domain.download.triggers.DownloadBook;
import br.com.somosdigital.bookshelf.domain.download.triggers.DownloadOed;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadUnzip;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.eventbus.BooksMessageEvent;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Device;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.domain.model.error.CustomErrorCodes;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.BookCheck;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadController implements DownloadCallback, DownloadStart {
    public static int BOOK_COMPLETE = 1;
    public static int BOOK_ACTIVITY = 2;
    public static int BOOK_OED = 3;

    private Book book;

    private BookSpecification bookSpecification;

    private DistribuidorService distribuidorService;

    private RequestCreator requestCreator;

    private DownloadUnzip downloadUnzip;

    private String publicKey;

    private Device device;

    private Log log;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private int oedListIndex = 0;

    public DownloadController(BookSpecification bookSpecification,
                              DistribuidorService distribuidorService,
                              RequestCreator requestCreator,
                              DownloadUnzip downloadUnzip,
                              String publicKey,
                              Device device,
                              Log log) {
        this.bookSpecification = bookSpecification;
        this.distribuidorService = distribuidorService;
        this.requestCreator = requestCreator;
        this.downloadUnzip = downloadUnzip;
        this.publicKey = publicKey;
        this.device = device;
        this.log = log;
    }

    @Override
    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public void downloadBook() {

        bookSpecification.openRealm();

        DownloadInformation.setCurrentBookId(book.getId());

        if(book.getDownloadType() == BOOK_OED) {
            book.setTotalBytesToDownload(0);
        }

        if (book.getDownloadStatus() != DownloadStatus.SUCCESSFUL
                && book.getDownloadStatus() != DownloadStatus.DECOMPRESSING
                && (book.getDownloadType() == BOOK_COMPLETE || book.getDownloadType() == BOOK_ACTIVITY)) {

            setBookPaths(book);
            book.setGlobalDownloadStatus(DownloadStatus.DOWNLOADING_BOOK_ACTIVITY);
            book.setKeyCryptography(publicKey);
            book.setDevice(device);

            MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            updateBookDatabase(book);

            DownloadBookListener downloadBookListener =
                    new DownloadBookListener(book, this, log);

            DownloadBook downloadBook = new DownloadBook
                    (
                            downloadBookListener,
                            distribuidorService,
                            this,
                            requestCreator,
                            compositeDisposable,
                            log
                    );

            downloadBook.prepareDownloadBook(book);

        } else {
            downloadActivity();
        }
    }

    @Override
    public void downloadActivity() {
        if(book.getActivity() != null
                && book.getActivity().getDownloadStatus() != DownloadStatus.SUCCESSFUL
                && (book.getDownloadType() == BOOK_COMPLETE || book.getDownloadType() == BOOK_ACTIVITY)
                && book.getActivity().getDownloadStatus() != DownloadStatus.DECOMPRESSING) {

            book.setGlobalDownloadStatus(DownloadStatus.DOWNLOADING_BOOK_ACTIVITY);
            book.getActivity().setKeyCryptography(publicKey);
            updateBookDatabase(book);
            MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

            setBookActivityPaths(book);
            FileManager.removeDirectory(book.getActivity().getActivityPath());
            FileManager.deleteFile(book.getActivity().getBrkPath());

            FileManager.createDirectory(book.getActivity().getActivityPath());

            DownloadActivityListener downloadActivityListener =
                    new DownloadActivityListener(book, book.getActivity(), this, log);

            DownloadActivity downloadActivity =
                    new DownloadActivity(
                            downloadActivityListener,
                            requestCreator,
                            distribuidorService,
                            this,
                            compositeDisposable,
                            log);

            book.setCurrentBytesDownloaded(book.getFileLength());
            DownloadInformation.calculateDownloadSize(book, book.getDownloadType());

            downloadActivity.prepareDownloadActivity(book.getActivity());

        } else if (book.getActivity() != null
                && book.getDownloadStatus() == DownloadStatus.DECOMPRESSING){
            unzipActivity(book.getActivity());

        } else {
            downloadOed();
        }
    }

    private void downloadOed() {
        if (book.getEducationalObjects() != null
                && oedListIndex < book.getEducationalObjects().size()
                && (book.getDownloadType() == BOOK_COMPLETE || book.getDownloadType() == BOOK_OED)) {

            if(book.getGlobalDownloadStatus() != DownloadStatus.DOWNLOADING_BOOK_OED) {
                book.setGlobalDownloadStatus(DownloadStatus.DOWNLOADING_BOOK_OED);
                updateBookDatabase(book);
                MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            }

            Oed oed = book.getEducationalObjects().get(oedListIndex);
            oedListIndex++;

            if (oed.getDownloadStatus() != DownloadStatus.SUCCESSFUL
                    && oed.getDownloadStatus() != DownloadStatus.DECOMPRESSING) {

                oed.setDownloadStatus(DownloadStatus.DOWNLOADING_BOOK_OED);

                oed.setKeyCryptography(publicKey);

                setOedPaths(oed);
                oed.setBookId(book.getId());

                DownloadInformation.setCurrentBookId(book.getId());

                DownloadOedListener downloadOedListener =
                        new DownloadOedListener(book, oed, this, log);

                DownloadOed downloadOed = new DownloadOed(downloadOedListener,
                        book,
                        requestCreator,
                        distribuidorService,
                        this,
                        compositeDisposable,
                        log);

                downloadOed.prepareDownloadOed(oed);

            } else if (oed.getDownloadStatus() == DownloadStatus.DECOMPRESSING) {
                unzipOed(oed);
            } else {
                downloadOed();
            }
        } else if (book.getEducationalObjects() != null
                && oedListIndex >= book.getEducationalObjects().size()
                && (book.getDownloadType() == BOOK_COMPLETE || book.getDownloadType() == BOOK_OED)) {

            for (Oed oed : book.getEducationalObjects()) {
                if (oed.getDownloadStatus() == DownloadStatus.FAILED) {
                    book.setGlobalDownloadStatus(DownloadStatus.FAILED);
                    break;
                }
            }

            if (book.getGlobalDownloadStatus() != DownloadStatus.FAILED) {
                if (book.getActivity() != null
                        && book.getActivity().getDownloadStatus() == DownloadStatus.FAILED) {
                    book.setGlobalDownloadStatus(DownloadStatus.FAILED);
                } else if (book.getDownloadStatus() == DownloadStatus.FAILED) {
                    book.setGlobalDownloadStatus(DownloadStatus.FAILED);
                } else {
                    book.setGlobalDownloadStatus(DownloadStatus.ALL_BOOK_SUCCESSFUL);
                }
            }

            if (book.getDownloadStatus() == DownloadStatus.UPDATE || (book.getActivity() != null
                    && book.getActivity().getDownloadStatus() == DownloadStatus.UPDATE)) {
                book.setGlobalDownloadStatus(DownloadStatus.UPDATE);
            }

            if (book.isUpdate()) {
                book.setGlobalDownloadStatus(DownloadStatus.UPDATE);
                book.setUpdate(false);
            }

            MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            DownloadInformation.setCurrentBookId("");
            nextBook();

        } else {

            if (book.getGlobalDownloadStatus() != DownloadStatus.FAILED) {
                if (book.getActivity() != null
                        && book.getActivity().getDownloadStatus() == DownloadStatus.FAILED) {
                    book.setGlobalDownloadStatus(DownloadStatus.FAILED);
                } else if (book.getDownloadStatus() == DownloadStatus.FAILED) {
                    book.setGlobalDownloadStatus(DownloadStatus.FAILED);
                } else {
                    if(BookCheck.containsOED(book)) {
                        book.setGlobalDownloadStatus(DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL);
                    } else {
                        book.setGlobalDownloadStatus(DownloadStatus.ALL_BOOK_SUCCESSFUL);
                    }
                }
            }

            if (book.isUpdate()) {
                book.setGlobalDownloadStatus(DownloadStatus.UPDATE);
                book.setUpdate(false);
            }

            MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            DownloadInformation.setCurrentBookId("");
            nextBook();

        }
    }

    public void updateBookDatabase(Book book) {
        bookSpecification.update(book);
    }

    private void setBookPaths(Book book) {
        //Book Paths
        book.setBookPath(BooksValues.getInstance().getBookRootPath() + File.separator + book.getId());
        book.setCompactedPath(BooksValues.getInstance().getBookRootPath() + File.separator + book.getId() + ".zip");
        book.setTempPath(BooksValues.getInstance().getBookRootPath() + File.separator + book.getId() + "_tmp");
        book.setBrkPath(book.getBookPath() + File.separator + book.getId() + ".brk");
    }

    private void setBookActivityPaths(Book book) {
        //Activity Paths
        String activityFolderPath = book.getBookPath();

        book.getActivity().setActivityPath(activityFolderPath + File.separator + "activities");
        book.getActivity().setCompactedPath(activityFolderPath + File.separator + book.getActivity().getId() + ".zip");
        book.getActivity().setTempPath(activityFolderPath + File.separator + book.getActivity().getId() + "_tmp");
        book.getActivity().setBrkPath(activityFolderPath + File.separator + "activity.brk");
    }

    private void setOedPaths(Oed oed) {
        //Oed Paths
        String oedFolderPath = book.getBookPath() + File.separator + "oeds";
        FileManager.createDirectory(oedFolderPath);

        oed.setOedPath(oedFolderPath + File.separator + oed.getId());
        oed.setCompactedPath(oedFolderPath + File.separator + oed.getId() + ".zip");
        oed.setTempPath(oedFolderPath + File.separator + oed.getId() + "_tmp");
        oed.setBrkPath(oedFolderPath + File.separator + oed.getId() + ".brk");
    }

    @Override
    public void nextBook() {
        oedListIndex = 0;
        updateBookDatabase(book);

        compositeDisposable.clear();

        if (DownloadInformation.getDownloadQueue().size() > 0
                && DownloadInformation.getCurrentBookId().equals("")) {

            DownloadInformation.getDownloadQueue().get(0).setInQueue(false);

            setBook(DownloadInformation.getDownloadQueue().get(0));

            downloadBook();

            DownloadInformation.getDownloadQueue().remove(0);

        } else {
            bookSpecification.closeRealm();
        }
    }

    @Override
    public void activityError(Activity activity) {
        DownloadInformation.setCurrentBookId("");
        activity.setDownloadStatus(DownloadStatus.FAILED);
        book.setGlobalDownloadStatus(DownloadStatus.FAILED);

        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        nextBook();
    }

    @Override
    public void bookError(Book book, String distribuidorErrorCode) {
        if (distribuidorErrorCode != null) {
            if (distribuidorErrorCode.contentEquals(CustomErrorCodes.DISTRIBUIDOR_DEVICE_LIMIT_REACHED)) {
                MainBus.getInstance().post(new BooksMessageEvent(CustomErrorCodes.DISTRIBUIDOR_DEVICE_LIMIT_REACHED));
            } else if (distribuidorErrorCode.contentEquals(CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED)) {
                MainBus.getInstance().post(new BooksMessageEvent(CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED));
            } else if (distribuidorErrorCode.contentEquals(CustomErrorCodes.BOOK_EXPIRED)) {
                MainBus.getInstance().post(new BooksMessageEvent(CustomErrorCodes.BOOK_EXPIRED));
                book.setExpiredDistribution(true);
            }
        }

        DownloadInformation.setCurrentBookId("");
        book.setDownloadStatus(DownloadStatus.FAILED);
        book.setGlobalDownloadStatus(DownloadStatus.FAILED);

        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        nextBook();
    }

    @Override
    public void oedError(Oed oed, String error) {
        boolean stopOedsDownload = false;

        if (error != null) {
            if (error.contentEquals(CustomErrorCodes.BOOK_EXPIRED)) {
                MainBus.getInstance().post(new BooksMessageEvent(CustomErrorCodes.BOOK_EXPIRED));
                book.setExpiredDistribution(true);
                stopOedsDownload =  true;
            }
        }

        DownloadInformation.setCurrentBookId("");
        oed.setDownloadStatus(DownloadStatus.FAILED);
        updateBookDatabase(book);

        if (!stopOedsDownload) {
            downloadOed();
        }
    }

    @Override
    public void unzipBook(Book book) {
        book.setGlobalDownloadStatus(DownloadStatus.DECOMPRESSING);
        book.setDownloadStatus(DownloadStatus.DECOMPRESSING);
        updateBookDatabase(book);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        compositeDisposable.add(
                downloadUnzip.unzipBook(book)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {},

                                throwable -> {

                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("unzipBook Error: " + throwable.getMessage());
                                    }

                                    bookError(book, null);

                                },

                                () -> {

                                    book.setDownloadStatus(DownloadStatus.SUCCESSFUL);
                                    updateBookDatabase(book);
                                    downloadActivity();
                                })
        );
    }

    @Override
    public void unzipActivity(Activity activity) {
        activity.setDownloadStatus(DownloadStatus.DECOMPRESSING);
        book.setGlobalDownloadStatus(DownloadStatus.DECOMPRESSING);
        updateBookDatabase(book);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        compositeDisposable.add(
                downloadUnzip.unzipActivity(activity)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                o -> {

                                },

                                throwable -> {
                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("unzipActivity Error: " + throwable.getMessage());
                                    }

                                    activityError(activity);

                                },

                                () -> {
                                    book.setGlobalDownloadStatus(DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL);
                                    activity.setDownloadStatus(DownloadStatus.SUCCESSFUL);

                                    downloadOed();
                                }
                        )

        );
    }

    @Override
    public void unzipOed(Oed oed) {
        book.setGlobalDownloadStatus(DownloadStatus.DECOMPRESSING);
        oed.setDownloadStatus(DownloadStatus.DECOMPRESSING);
        updateBookDatabase(book);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        compositeDisposable.add(
                downloadUnzip.unzipOed(oed)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                o -> {

                                },

                                throwable -> {
                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("unzipOed Error: " + throwable.getMessage());
                                    }

                                    oedError(oed, null);
                                },

                                () -> {
                                    oed.setDownloadStatus(DownloadStatus.SUCCESSFUL);
                                    downloadOed();
                                }
                        )

        );
    }
}
