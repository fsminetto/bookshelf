package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.implementation;

/**
 * Created by jgsmanaroulas on 11/1/16.
 */
public interface ISendActivity {
    void sendActivity(String activity, String statement);
}
