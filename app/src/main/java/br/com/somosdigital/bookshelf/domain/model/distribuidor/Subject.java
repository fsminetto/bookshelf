package br.com.somosdigital.bookshelf.domain.model.distribuidor;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pelisoli on 10/10/16.
 */
public class Subject extends RealmObject {
    @PrimaryKey
    @Expose()
    private String id;

    @Expose()
    private String name;

    @Expose()
    @SerializedName("external_id")
    private String externalId;

    public Subject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
