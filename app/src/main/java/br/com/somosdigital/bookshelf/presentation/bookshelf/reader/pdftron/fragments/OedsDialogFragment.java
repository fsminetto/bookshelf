//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2016 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.adapter.adapter.OedListAdapter;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;

public class OedsDialogFragment extends DialogFragment {

    public static final String TAG = "OEDSDIALOGFRAGMENT";

    public static final String BUNDLE_OEDS = "oeds";

    private OedsDialogFragmentListener mListener;

    private ArrayList<OedInfo> oedList;

    ListView oedsListView;

    ImageView imvClose;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static OedsDialogFragment newInstance(OedsDialogFragment.OedsDialogFragmentListener listener) {
        OedsDialogFragment f = new OedsDialogFragment();
        f.setListener(listener);
        return f;
    }

    public void setListener(OedsDialogFragmentListener listener){
        this.mListener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.book_oeds_list_layout, null);
        setupViews(view);

        oedList = getArguments().getParcelableArrayList(BUNDLE_OEDS);

        OedListAdapter adapter = new OedListAdapter(this.getActivity(), R.layout.book_oeds_item, oedList);

        oedsListView.setAdapter(adapter);
        oedsListView.setOnItemClickListener((parent, view1, position, id) -> {
            OedInfo oedId = oedList.get(position);
                if (mListener != null) {
                    mListener.onOedSelected(oedId);
                    dismissAllowingStateLoss();
                }
        });
        return view;
    }

    private void setupViews(View view) {
        oedsListView = view.findViewById(R.id.book_oeds_listview);
        imvClose = view.findViewById(R.id.book_oeds_imv_close);
        imvClose.setOnClickListener(view1 -> dismissAllowingStateLoss());
    }

    public interface OedsDialogFragmentListener {

        void onOedSelected(OedInfo oedInfo);

    }
}
