package br.com.somosdigital.bookshelf.domain.eventbus;

/**
 * Created by marcus on 20/04/17.
 */

public class BooksMessageEvent {
    private String errorCode;

    public BooksMessageEvent(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
