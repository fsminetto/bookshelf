package br.com.somosdigital.bookshelf.storage.module;

import br.com.somosdigital.bookshelf.BuildConfig;
import io.realm.RealmConfiguration;

/**
 * Created by pelisoli on 8/30/17.
 */

public class BookshelfDatabase {

    private static RealmConfiguration realmConfig = null;

    public static RealmConfiguration getRealmConfig() {
        if (realmConfig == null) {
            realmConfig = new RealmConfiguration.Builder()
                    .name("bookshelf.realm")
                    .modules(new BookshelfModule())
                    .schemaVersion(BuildConfig.BOOKSHELF_DATABASE_VERSION)
                    .migration(new BookshelfMigration())
                    .build();
        }

        return realmConfig;
    }
}
