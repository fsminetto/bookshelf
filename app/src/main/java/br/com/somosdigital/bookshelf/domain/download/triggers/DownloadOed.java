package br.com.somosdigital.bookshelf.domain.download.triggers;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.converge.download.android.DownloadRequest;
import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.domain.model.error.CustomErrorCodes;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadOed {

    private DownloadStatusListener downloadStatusListener;

    private final Book book;

    private RequestCreator requestCreator;

    private DistribuidorService service;

    private DownloadCallback downloadCallback;

    private CompositeDisposable compositeDisposable;

    private Log log;


    public DownloadOed(DownloadStatusListener downloadStatusListener,
                       Book book,
                       RequestCreator requestCreator,
                       DistribuidorService service,
                       DownloadCallback downloadCallback,
                       CompositeDisposable compositeDisposable,
                       Log log){
        this.downloadStatusListener = downloadStatusListener;
        this.book = book;
        this.requestCreator = requestCreator;
        this.service = service;
        this.downloadCallback = downloadCallback;
        this.compositeDisposable = compositeDisposable;
        this.log = log;
    }

    public void prepareDownloadOed(Oed oed) {
        if (oed != null) {

            compositeDisposable.add(
                    service.getOedFile(oed)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(

                                    oed1 -> {
                                        oed.setFilePath(oed1.getFilePath());
                                        oed.setPassword(oed1.getPassword());

                                        DownloadInformation.calculateDownloadSize(book, book.getDownloadType());

                                        downloadOed(oed);
                                    },

                                    throwable -> {
                                        if (throwable != null && throwable.getMessage() != null) {
                                            log.logError("prepareDownloadOed Error: " + throwable.getMessage());
                                        }

                                        if (throwable instanceof HttpException &&
                                                ((HttpException) throwable).code() == 400){

                                            Response response = ((HttpException)throwable).response();
                                            JSONObject jObjError = null;

                                            try {
                                                jObjError = new JSONObject(response.errorBody().string());
                                                if (jObjError.has("Code")){
                                                    String errorCode = jObjError.getString("Code");

                                                    if (errorCode.contentEquals(CustomErrorCodes.BOOK_EXPIRED)) {
                                                        book.setExpiredDistribution(true);
                                                        downloadCallback.oedError(oed, CustomErrorCodes.BOOK_EXPIRED);
                                                    }
                                                }
                                            } catch (JSONException | IOException e) {
                                                e.printStackTrace();
                                                log.logError("prepareDownloadBook Error: " + e.getMessage());
                                            }

                                        }else{
                                            downloadCallback.oedError(oed, null);
                                        }

                                    }
                            )
            );
        }
    }

    private void downloadOed(Oed oed) {
        if (oed != null) {
            DownloadRequest downloadRequest = requestCreator.createDownloadRequest(oed.getFilePath(),
                    oed.getCompactedPath(), downloadStatusListener);

            DownloadInformation.setCurrentDownloadId(requestCreator.getDownloadManager().add(downloadRequest));
        }
    }
}
