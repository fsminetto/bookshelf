package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by marcus.martins on 06/09/16.
 */
public class Activity extends RealmObject {
    @PrimaryKey
    @Expose()
    private String id;

    @Expose()
    @SerializedName(value = "file_length")
    private long fileLength;

    @Expose()
    @Ignore
    private String filePath;

    @Expose()
    @Ignore
    private String keyCryptography;

    @Expose()
    @Ignore
    private String password;

    private int downloadStatus;

    private String activityPath;

    private String compactedPath;

    private String tempPath;

    private String brkPath;

    @Expose()
    private int version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getKeyCryptography() {
        return keyCryptography;
    }

    public void setKeyCryptography(String keyCryptography) {
        this.keyCryptography = keyCryptography;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public DownloadStatus getDownloadStatus() {
        return DownloadStatus.fromInt(this.downloadStatus);
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        if (downloadStatus != null) {
            this.downloadStatus = downloadStatus.getId();
        }
    }

    public String getActivityPath() {
        return activityPath;
    }

    public void setActivityPath(String activityPath) {
        this.activityPath = activityPath;
    }

    public String getCompactedPath() {
        return compactedPath;
    }

    public void setCompactedPath(String compactedPath) {
        this.compactedPath = compactedPath;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getBrkPath() {
        return brkPath;
    }

    public void setBrkPath(String brkPath) {
        this.brkPath = brkPath;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
