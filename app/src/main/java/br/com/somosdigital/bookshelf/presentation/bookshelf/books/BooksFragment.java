package br.com.somosdigital.bookshelf.presentation.bookshelf.books;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.domain.openid.TokenManager;
import br.com.solucaoadapta.shared.presentation.dialog.information.InformationDialog;
import br.com.somosdigital.bookshelf.BuildConfig;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BookListUtil;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BooksValues;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.DensityHelper;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.DeviceHelper;
import br.com.somosdigital.bookshelf.domain.cryptography.CryptographyManager;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadController;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadStart;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadUnzip;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadUnzipImpl;
import br.com.somosdigital.bookshelf.domain.eventbus.BooksMessageEvent;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Device;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import br.com.somosdigital.bookshelf.domain.model.error.CustomErrorCodes;
import br.com.somosdigital.bookshelf.domain.utils.MarginDecoration;
import br.com.somosdigital.bookshelf.domain.utils.state.ApiState;
import br.com.somosdigital.bookshelf.domain.utils.state.BookshelfLibraryState;
import br.com.somosdigital.bookshelf.domain.utils.view.AutoFitRecyclerView;
import br.com.somosdigital.bookshelf.presentation.bookshelf.BookshelfActivityCommunication;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.adapters.BooksAdapter;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.CardViewHolder;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.menu.BookPopupMenu;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.menu.PopupMenuListener;
import br.com.somosdigital.bookshelf.storage.repository.book.BookRepository;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;
import br.com.somosdigital.bookshelf.storage.repository.template.TemplateRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by marcus on 01/12/16.
 */
public class BooksFragment extends Fragment implements BooksFragmentContract.View {

    private AutoFitRecyclerView recyclerView;

    private ProgressWheel progressBar;

    private ImageView imageBooksError;

    private TextView textBooksError;

    private BooksAdapter adapter;

    private SwipeRefreshLayout swipeContainer;

    private String screenDensity;

    private BooksFragmentPresenter presenter;

    private BookSpecification bookSpecification = null;

    private Device device;

    private CompositeDisposable compositeDisposable = null;

    private BookshelfActivityCommunication bookshelfActivityCommunication = null;

    private DownloadActions downloadActions;

    private DownloadUnzip downloadUnzip;

    private Log log;

    private BookshelfUser user;

    private String userId;

    private List<Book> filteredBookList;

    private List<Book> searchBookList;

    private String searchText;

    private String subjectFilterId;

    private boolean showNoActivityIcon;

    public static BooksFragment newInstance(String userId, Log log) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("log", log);
        bundle.putString("userId", userId);

        BooksFragment fragment = new BooksFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    public static BooksFragment newInstance(String userId, Log log, boolean showNoActivityIcon) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("log", log);
        bundle.putString("userId", userId);
        bundle.putBoolean("showNoActivityIcon", showNoActivityIcon);

        BooksFragment fragment = new BooksFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBasicInfo(getArguments());
        return inflater.inflate(R.layout.bookshelf_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setUpViews(device, view);

        downloadInstancesCreation();

        presenter = new BooksFragmentPresenter
                (
                        ApiState.getDistribuidorService(),
                        downloadUnzip,
                        log
                );

        presenter.attachView(this);
        presenter.checkTemplateUpdate(
                ApiState.getTemplateService(getContext()),
                new TemplateRepository(),
                BookshelfLibraryState.getTemplateDownloadController(),
                createTemplate());
        user = presenter.loadUser(userId);
        presenter.loadBooks(user, device.getDeviceId(), screenDensity);
    }

    private void setUpViews(Device device, View view){
        imageBooksError = view.findViewById(R.id.icon_error);
        textBooksError = view.findViewById(R.id.txvError);

        progressBar = view.findViewById(R.id.progress_loading_books);
        progressBar.setVisibility(View.VISIBLE);

        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> {
            presenter.loadBooks(user, device.getDeviceId(), screenDensity);
        });

        swipeContainer.setColorSchemeResources(br.com.solucaoadapta.shared.R.color.colorPrimary,
                br.com.solucaoadapta.shared.R.color.colorPrimaryDark,
                br.com.solucaoadapta.shared.R.color.colorAccent);

        adapter = new BooksAdapter(getActivity(), BookListUtil.getBookList(), this, showNoActivityIcon);

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(500);
        alphaAdapter.setFirstOnly(false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(alphaAdapter);

    }

    @Override
    public void showBooks(List<Book> books) {

        if (books != null && books.size() > 0) {
            BookListUtil.getBookList().clear();
            BookListUtil.getBookList().addAll(books);
            BookListUtil.getBookList().get(0).setExpires(false);

            adapter.addNewBooks(books);

            presenter.searchBySubject(BookListUtil.getBookList(), subjectFilterId);

            progressBar.setVisibility(View.INVISIBLE);
            swipeContainer.setRefreshing(false);

            imageBooksError.setVisibility(View.INVISIBLE);
            textBooksError.setVisibility(View.INVISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            BookListUtil.getBookList().clear();
            adapter.addNewBooks(books);

            progressBar.setVisibility(View.INVISIBLE);
            swipeContainer.setRefreshing(false);

            if (books != null && books.size() == 0) {
                textBooksError.setText(getResources().getText(R.string.no_books));
            }

            imageBooksError.setVisibility(View.VISIBLE);
            textBooksError.setVisibility(View.VISIBLE);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void showFilterBooks(List<Book> bookList) {
        if (bookList != null) {
            if (searchBookList != null) {
                searchBookList.clear();
                searchBookList.addAll(bookList);
            }else{
                searchBookList = new ArrayList<>();
                searchBookList.addAll(bookList);
            }

            if(bookList.size() > 0){
                recyclerView.setVisibility(View.VISIBLE);
                adapter.addNewBooks(searchBookList);
                imageBooksError.setVisibility(View.INVISIBLE);
                textBooksError.setVisibility(View.INVISIBLE);
            } else {
                recyclerView.setVisibility(View.GONE);
                //imageBooksError.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_no_book));
                textBooksError.setText(getResources().getText(R.string.no_books_filter));
                imageBooksError.setVisibility(View.VISIBLE);
                textBooksError.setVisibility(View.VISIBLE);
            }
        } else {
            recyclerView.setVisibility(View.GONE);
            //imageBooksError.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_no_book));
            textBooksError.setText(getResources().getText(R.string.no_books_filter));
            imageBooksError.setVisibility(View.VISIBLE);
            textBooksError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showFilterSubjects(List<Book> bookList) {
        if (bookList != null) {
            if (filteredBookList != null) {
                filteredBookList.clear();
                filteredBookList.addAll(bookList);
            }else{
                filteredBookList = new ArrayList<>();
                filteredBookList.addAll(bookList);
            }

            presenter.searchByTitle(bookList, searchText);
        }
    }

    @Override
    public void openBook(Book book) {
        boolean hasActivities = book.getActivity() != null;
        bookshelfActivityCommunication.startReader(book, hasActivities);
    }

    @Override
    public void onDeviceLimitReachError() {
        InformationDialog informationDialog = InformationDialog
                .newInstance(getString(R.string.bookshelf_device_limit_reached));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onDeviceBlockedError() {
        InformationDialog informationDialog = InformationDialog
                .newInstance(getString(R.string.bookshelf_device_blocked));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBookExpired() {
        InformationDialog informationDialog = InformationDialog
                .newInstance(getString(R.string.book_expired));

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(informationDialog, "InfoDialog");
        ft.commitAllowingStateLoss();
    }

    @Override
    public void showBookMenu(CardViewHolder cardViewHolder, Book book) {
        PopupMenu popupMenu = BookPopupMenu.createMenu(getContext(), cardViewHolder.actionMenu, book);
        popupMenu.setOnMenuItemClickListener(
                new PopupMenuListener(getContext(), downloadActions, book, bookshelfActivityCommunication));
        popupMenu.show();
    }

    @Override
    public void onResume(){
        super.onResume();

        compositeDisposable.add(MainBus.getInstance().getBusObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    if (o instanceof UpdateViewEvent) {
                        UpdateViewEvent event = (UpdateViewEvent) o;
                        adapter.notifyItemChanged(event.getPosition());
                    }

                    if (o instanceof BooksMessageEvent) {
                        BooksMessageEvent event = (BooksMessageEvent) o;

                        switch (event.getErrorCode()) {
                            case CustomErrorCodes.DISTRIBUIDOR_DEVICE_LIMIT_REACHED:
                                onDeviceLimitReachError();
                                break;
                            case CustomErrorCodes.DISTRIBUIDOR_DEVICE_BLOCKED:
                                onDeviceBlockedError();
                                break;
                            case CustomErrorCodes.BOOK_EXPIRED:
                                onBookExpired();
                                break;
                        }
                    }
                }));
    }

    public void downloadAllBooks(){
        downloadActions.startDownloadAllBooks(BookListUtil.getBookList(), false);
        adapter.notifyDataSetChanged();
    }

    public void downloadEverything(){
        downloadActions.startDownloadAllBooks(BookListUtil.getBookList(), true);
        adapter.notifyDataSetChanged();
    }

    public void searchBook(CharSequence text) {
        if (text != null) {
            searchText = text.toString();
            presenter.searchByTitle(filteredBookList, text.toString());
        }
    }

    public void filterBySubject(String text) {
        if (text != null) {
            subjectFilterId = text;
            presenter.searchBySubject(BookListUtil.getBookList(), subjectFilterId);
        }
    }

    public static void setTokenManager(TokenManager tokenManager){
        ApiState.setTokenManager(tokenManager);
    }

    @Override
    public void showServerMaintenanceError() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            bookshelfActivityCommunication = (BookshelfActivityCommunication) getActivity();
        }
    }

    private Template createTemplate(){
        Template template = new Template();
        template.setDownloadPath(BuildConfig.TEMPLATE_HTTP + BuildConfig.TEMPLATE_VERSION + "/templates.zip");
        template.setSourcePath(BooksValues.getInstance().getTemplatePath());
        template.setZipPath(BooksValues.getInstance().getBookRootPath() + File.separator + "templates.zip");
        template.setDestPath(BooksValues.getInstance().getBookRootPath() + File.separator + "templates");
        template.setTempPath(BooksValues.getInstance().getBookRootPath() + File.separator + "temp");

        return template;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(presenter != null) {
            presenter.detachView();
        }

        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    private void getBasicInfo(Bundle bundle){
        if (bundle != null) {
            log = bundle.getParcelable("log");
            userId = bundle.getString("userId");
            showNoActivityIcon = bundle.getBoolean("showNoActivityIcon", false);
        }

        compositeDisposable = new CompositeDisposable();
        device = DeviceHelper.getDevice(getActivity());
        screenDensity = DensityHelper.getDensityName(getActivity());
    }

    private void downloadInstancesCreation() {

        if (bookSpecification == null) {
            bookSpecification = new BookRepository();
        }

        downloadUnzip = new DownloadUnzipImpl(
                log,
                getContext(),
                BookshelfLibraryState.getPreferenceName()
        );

        DownloadStart downloadStart = new DownloadController(
                bookSpecification,
                ApiState.getDistribuidorService(),
                BookshelfLibraryState.getBookshelfRequestCreator(),
                downloadUnzip,
                CryptographyManager.getPublicKey(getContext(), BookshelfLibraryState.getPreferenceName()),
                DeviceHelper.getDevice(getContext()),
                log
        );

        downloadActions = new DownloadActions(
                bookSpecification,
                downloadStart,
                BookshelfLibraryState.getBookshelfRequestCreator(),
                log
        );
    }

}