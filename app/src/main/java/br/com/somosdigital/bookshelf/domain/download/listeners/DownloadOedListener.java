package br.com.somosdigital.bookshelf.domain.download.listeners;

import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadOedListener implements DownloadStatusListener {
    private Book book;

    private Oed oed;

    private DownloadCallback downloadCallback;

    private Log log;

    private int checkProgress = 0;

    private long lastDownloadedBytes = 0;

    public DownloadOedListener(Book book, Oed oed, DownloadCallback downloadCallback, Log log) {
        this.book = book;
        this.oed = oed;
        this.downloadCallback = downloadCallback;
        this.log = log;
    }

    @Override
    public void onDownloadComplete(int id) {
        log.logInfo("Download Oed Complete");
        downloadCallback.unzipOed(oed);
    }

    @Override
    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
        downloadCallback.oedError(oed, errorMessage);
    }

    @Override
    public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {
        if (checkProgress + 2 == progress) {

            book.setCurrentBytesDownloaded(book.getCurrentBytesDownloaded()
                    + (downloadedBytes - lastDownloadedBytes));

            if(book.getCardProgress() != null) {
                book.getCardProgress()
                        .onProgressUpdate((DownloadInformation.getProgress(book)));
            }

            checkProgress = progress;
            lastDownloadedBytes = downloadedBytes;
        }
    }

    @Override
    public void onPause(int id, long downloadedBytes) {
        log.logInfo("Download Oed Pause");

        DownloadInformation.setCurrentBookId("");
        oed.setDownloadStatus(DownloadStatus.PAUSED);
        book.setGlobalDownloadStatus(DownloadStatus.PAUSED);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        downloadCallback.nextBook();
    }

    @Override
    public void onDownloadCanceled(int id) {
        log.logInfo("Download Oed Canceled");

        book.setGlobalDownloadStatus(DownloadStatus.CANCELING);
        oed.setDownloadStatus(DownloadStatus.CANCELING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        DownloadInformation.setCurrentBookId("");

        removeOedFiles();

        for (Oed oed: book.getEducationalObjects()) {
            oed.setDownloadStatus(DownloadStatus.NEW);
        }

        book.setDownloadStatus(DownloadStatus.SUCCESSFUL);


        float totalDownloadedSize = 0;

        totalDownloadedSize += book.getFileLength();

        if (book.getActivity() != null && book.getActivity().getDownloadStatus() == DownloadStatus.SUCCESSFUL){
            totalDownloadedSize += book.getActivity().getFileLength();
        }

        book.setCurrentBytesDownloaded(totalDownloadedSize);

        book.setGlobalDownloadStatus(DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        downloadCallback.nextBook();
    }

    private void removeOedFiles() {
        if (book.getEducationalObjects() != null){
            for (Oed oed : book.getEducationalObjects()){
                if (oed.getCompactedPath() != null) {
                    FileManager.deleteFile(oed.getCompactedPath());
                }
                if (oed.getTempPath() != null) {
                    FileManager.removeDirectory(oed.getTempPath());
                }
                if (oed.getBrkPath() != null) {
                    FileManager.deleteFile(oed.getBrkPath());
                }
                if (oed.getOedPath() != null){
                    FileManager.removeDirectory(oed.getOedPath());
                }
            }
        }
    }
}
