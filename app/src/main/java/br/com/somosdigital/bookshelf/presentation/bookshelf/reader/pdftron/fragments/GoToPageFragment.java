package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import br.com.somosdigital.bookshelf.R;

/**
 * Created by jgsmanaroulas on 07/02/17.
 */
public class GoToPageFragment extends DialogFragment {

    public static final String TAG = "GOTOPAGEFRAGMENT";

    public interface GoToPageListener {
        void onPageSelected(int page);
    }

    TextView txvCancel;

    TextView txvGo;

    EditText edtPage;

    private GoToPageListener listener;

    public static GoToPageFragment newInstance(GoToPageListener listener) {
        GoToPageFragment f = new GoToPageFragment();
        f.setListener(listener);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.goto_page_fragment_layout, null);
        setupViews(view);
        edtPage.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                onPageSelected();
            }
            return false;
        });

        return view;
    }

    private void setupViews(View view) {
        txvCancel = view.findViewById(R.id.goto_page_btn_cancel);
        txvGo = view.findViewById(R.id.goto_page_btn_go);
        edtPage = view.findViewById(R.id.goto_page_edt);

        txvCancel.setOnClickListener(view1 -> dismissAllowingStateLoss());
        txvGo.setOnClickListener(view12 -> onPageSelected());

    }

    public void setListener(GoToPageListener listener){
        this.listener = listener;
    }

    private void onPageSelected(){
        if (listener != null){
            if (!edtPage.getText().toString().isEmpty()) {
                try {
                    int page = Integer.parseInt(edtPage.getText().toString());
                    listener.onPageSelected(page);
                } catch (NumberFormatException ex){
//                    ((MainApplication)getActivity().getApplication()).getLog().logError(ex.getMessage());
                }
            }
        }
        dismissAllowingStateLoss();
    }

}
