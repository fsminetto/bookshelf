package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces;

/**
 * Created by luis.afonso on 13/07/2015.
 */
public interface ReaderOED {

    boolean playMedia(String oedMediaPath);

}
