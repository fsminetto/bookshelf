package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.activities_list;

import java.util.ArrayList;

import br.com.solucaoadapta.shared.presentation.base.MvpPresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;

/**
 * Created by marcus on 29/03/17.
 */

public interface ActivitiesDialogContract {
    interface View extends MvpView {
        void showActivities(ArrayList<BookActivityInfo> activities);
    }

    interface Presenter extends MvpPresenter<View> {
        void orderActivitiesByName(ArrayList<BookActivityInfo> activities);
    }
}
