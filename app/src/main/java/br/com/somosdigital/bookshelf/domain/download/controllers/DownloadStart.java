package br.com.somosdigital.bookshelf.domain.download.controllers;


import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by marcus on 27/04/17.
 */

public interface DownloadStart {
    void setBook(Book book);

    void downloadBook();
}
