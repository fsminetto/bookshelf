package br.com.somosdigital.bookshelf.domain.utils.state;

import br.com.converge.download.android.ThinDownloadManager;
import br.com.somosdigital.bookshelf.BuildConfig;
import br.com.somosdigital.bookshelf.domain.bookshelf.utils.BookListUtil;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreatorImpl;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.DownloadActions;
import io.reactivex.Observable;

/**
 * Created by pelisoli on 9/20/17.
 */
public class BookshelfLibraryState {

    private static RequestCreator downloadController;

    private static RequestCreator templateDownloadController;

    private static BookshelfUser bookshelfUser;

    public static RequestCreator getBookshelfRequestCreator(){
        if(downloadController == null){
            downloadController = new RequestCreatorImpl(new ThinDownloadManager());
        }

        return downloadController;
    }

    public static RequestCreator getTemplateDownloadController(){
        if(templateDownloadController == null){
            templateDownloadController = new RequestCreatorImpl(new ThinDownloadManager());
        }

        return templateDownloadController;
    }

    public static String getPreferenceName(){
        return BuildConfig.BOOKSHELF_PREF_NAME;
    }

    public static Observable<Boolean> logout(){
        return new DownloadActions(getBookshelfRequestCreator()).pauseDownloadBook();
    }

    public static void clear(){
        downloadController = null;
        templateDownloadController = null;
        bookshelfUser = null;
        BookListUtil.getBookList().clear();
    }

    public static BookshelfUser getBookshelfUser(){
        return bookshelfUser;
    }

    public static void setBookshelfUser(BookshelfUser newBookshelfUser){
        bookshelfUser = newBookshelfUser;
    }
}
