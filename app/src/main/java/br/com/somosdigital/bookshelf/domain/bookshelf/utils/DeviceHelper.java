package br.com.somosdigital.bookshelf.domain.bookshelf.utils;

import android.content.Context;

import br.com.converge.deviceid.android.DeviceId;
import br.com.converge.deviceid.android.MyDeviceInfo;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Device;

/**
 * Created by pelisoli on 05/04/16.
 */
public class DeviceHelper {

    public static Device getDevice(Context context) {

        Device device = new Device();
        device.setDeviceId(new DeviceId().generateID(context));
        device.setDeviceModel(MyDeviceInfo.getFriendlyDeviceName());
        device.setOperationalSystem("Android");
        device.setScreenArea(MyDeviceInfo.getScreenResolution(context));
        device.setUserSystem("X");
        device.setVersionSystem(MyDeviceInfo.getAndroidVersion());

        return device;
    }
}
