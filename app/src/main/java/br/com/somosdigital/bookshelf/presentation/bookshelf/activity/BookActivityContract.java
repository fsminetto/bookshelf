package br.com.somosdigital.bookshelf.presentation.bookshelf.activity;

import br.com.solucaoadapta.shared.presentation.base.MvpPresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.domain.activity.AdaptaActivity;

/**
 * Created by pelisoli on 9/26/17.
 */

public interface BookActivityContract {

    interface View extends MvpView {

        void loadActivity(String finalTemplateUrl);

        void showNotificationError();

        void hideSendActivityProgress();

        void showSendActivitySuccess();

        void showSendActivityError();

        void showSendViewSuccess();

         void showSendError();
    }

    interface Presenter extends MvpPresenter<View> {

        void prepareActivity(AdaptaActivity adaptaActivity);


        void saveActivity(String statements, boolean keepOpen);

        String getClassInfo(Object object,
                            AdaptaActivity adaptaActivity,
                            String token,
                            String userId,
                            boolean isTeacher);

    }

}
