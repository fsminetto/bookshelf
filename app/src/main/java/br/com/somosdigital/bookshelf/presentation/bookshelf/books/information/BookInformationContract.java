package br.com.somosdigital.bookshelf.presentation.bookshelf.books.information;

import br.com.solucaoadapta.shared.presentation.base.MvpPresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by michelribeiro on 01/11/17.
 */

public class BookInformationContract {

    public interface View extends MvpView {

        void showLoadingBookInfo(boolean show);

        void showBook(Book book);
    }


    public interface Presenter extends MvpPresenter<View> {

        void loadBook(String bookId);

        String getReadableFileSize(long bytes);
    }
}
