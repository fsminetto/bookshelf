package br.com.somosdigital.bookshelf.presentation.bookshelf.activity;

/**
 * Created by pelisoli on 11/1/17.
 */

public interface ReaderActivity {

    void saveActivity(String statements);

    String onGetClassInfo();

}
