package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model;

/**
 * Created by jgsmanaroulas on 12/04/17.
 */
public class FreeHandProperties {

    private final int color;
    private final int fill;
    private final float thickness;
    private final float opacity;
    private final String icon;
    private final String pdfTronFontName;

    public FreeHandProperties(int color, int fill, float thickness, float opacity, String icon, String pdfTronFontName){
        this.color = color;
        this.fill = fill;
        this.thickness = thickness;
        this.opacity = opacity;
        this.icon = icon;
        this.pdfTronFontName = pdfTronFontName;
    }

    public int getColor() {
        return color;
    }

    public int getFill() {
        return fill;
    }

    public float getThickness() {
        return thickness;
    }

    public float getOpacity() {
        return opacity;
    }

    public String getIcon() {
        return icon;
    }

    public String getPdfTronFontName() {
        return pdfTronFontName;
    }

}
