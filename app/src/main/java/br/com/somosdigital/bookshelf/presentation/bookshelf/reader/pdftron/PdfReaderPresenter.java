package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.somosdigital.bookshelf.domain.bookshelf.reader.PdfReaderInteractor;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by jgsmanaroulas on 20/01/17.
 */
public class PdfReaderPresenter extends BasePresenter<PdfReaderContract.View>
        implements PdfReaderContract.Presenter {

    private Log log;
    private PdfReaderInteractor interactor;


    public PdfReaderPresenter(Log log,
                              PdfReaderInteractor interactor) {
        this.log = log;
        this.interactor = interactor;
    }

    @Override
    public void loadPDFDocument(String bookUrl) {
        getView().safeExecute(PdfReaderContract.View::onDocumentLoading);

        if (interactor.checkIfDocumentExists()) {
            int lastPage = getLastReadPage();
            getView().safeExecute(v -> v.onLocalDocumentReady(lastPage));
        } else if (bookUrl != null && !bookUrl.equals("")) {
            int lastPage = getLastReadPage();
            getView().safeExecute(v -> v.openFromUrl(bookUrl, "", lastPage));
        } else {
            getDocumentUrl();
        }
    }

    private void getDocumentUrl() {
        if (interactor != null) {
            addSubscription(interactor.getBookFile()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            bookFile -> {
                                int lastPage = getLastReadPage();
                                getView().safeExecute(
                                        v -> v.openFromUrl(
                                                bookFile.getFilePath(),
                                                bookFile.getKeyCryptography(),
                                                lastPage));
                            },
                            throwable -> {
                                if (throwable != null && throwable.getMessage() != null) {
                                    log.logError("prepareDownloadBook Error: " + throwable.getMessage());
                                }
                                getView().safeExecute(PdfReaderContract.View::showLoadingDocumentError);
                            }
                    ));
        }
    }

    @Override
    public void preloadActivities() {
        try {
            HashMap<Integer, List<BookActivityInfo>> activityMap = interactor.getActivityMap();
            getView().safeExecute(view -> view.setActivityMap(activityMap));
        } catch (JSONException e) {
            log.logError(e.getMessage());
            getView().safeExecute(PdfReaderContract.View::onLoadActivitiesError);
            e.printStackTrace();
        }
    }

    @Override
    public void preloadOeds() {
        List<OedInfo> oeds = interactor.getOedList();

        if (oeds.size() > 0) {
            getView().safeExecute(view -> view.onOedsLoaded(oeds));
        } else {
            getView().safeExecute(PdfReaderContract.View::onNoOedsLoaded);
        }
    }

    @Override
    public String getBookFilePath(String id) {
        return interactor.getBookFilePath(id);
    }

    @Override
    public String getDocumentPassword(DrmClient drmClient) {

        String password = interactor.getDocumentPassword(drmClient);

        if (password == null) {
            getView().safeExecute(PdfReaderContract.View::onPasswordError);
        }

        return password;
    }

    @Override
    public void saveLastReadPage(int currentPage) {
        interactor.saveLastReadPage(currentPage);
    }

    @Override
    public void loadPageActivities(int[] visiblePages, HashMap<Integer, List<BookActivityInfo>> activityMap) {
        List<BookActivityInfo> activitiesList = interactor.loadPageActivities(visiblePages, activityMap);
        if (activitiesList != null)
            getView().safeExecute(v -> v.onActivitiesLoaded(activitiesList));
    }

    private int getLastReadPage() {
        return interactor.getLastReadPage();
    }

    @Override
    public void downloadXFDF() {
        addSubscription(interactor.downloadAnnotationFile()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> {
                    if (responseBody.isSuccessful()) {
                        getView().safeExecute(PdfReaderContract.View::importAnnot);
                        getView().safeExecute(PdfReaderContract.View::clearCommandsLists);
                    } else {
                        getView().safeExecute(PdfReaderContract.View::unsetFirstLoad);
                    }
                }, throwable -> {
                    getView().safeExecute(PdfReaderContract.View::loadBackupAnnots);
                    getView().safeExecute(PdfReaderContract.View::showAnnotationDownloadError);
                    log.logError("DEU RUIM NO download!");
                    log.logError(throwable.getLocalizedMessage());
                })
        );
    }

    @Override
    public void sendBackupAnnotations(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands) {
        getView().safeExecute(PdfReaderContract.View::startSendingAnnotUpdate);

        String xfdfCommand = interactor.createXFDFString(addCommands, modifyCommands, removeCommands);
        addSubscription(interactor
                .sendAnnotations(xfdfCommand)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Response<ResponseBody> responseBodyResponse) -> {
                            getView().safeExecute(view -> view.setHasError(false));
                            discardAnnotationsBackup();
                        },
                        throwable -> {
                            log.logError(throwable.getMessage());
                            getView().safeExecute(view -> view.setHasError(true));
                        }));
    }

    @Override
    public void sendAnnotaionUpdate(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands) {
        getView().safeExecute(PdfReaderContract.View::startSendingAnnotUpdate);

        String xfdfCommand = interactor.createXFDFString(addCommands, modifyCommands, removeCommands);
        addSubscription(interactor
                .sendAnnotations(xfdfCommand)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Response<ResponseBody> responseBodyResponse) -> {
                            getView().safeExecute(view -> view.setHasError(false));
                            getView().safeExecute(PdfReaderContract.View::clearCommandsLists);
                            discardAnnotationsBackup();
                        },
                        throwable -> {
                            log.logError(throwable.getMessage());
                            getView().safeExecute(view -> view.setHasError(true));
                        }));
    }

    @Override
    public void sendAnnotationsThenClose(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands) {
        getView().safeExecute(PdfReaderContract.View::startSendingAnnotUpdate);
        String xfdfCommand = interactor.createXFDFString(addCommands, modifyCommands, removeCommands);
        addSubscription(interactor
                .sendAnnotations(xfdfCommand)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBodyResponse -> {
                            getView().safeExecute(view -> view.setHasError(false));
                            getView().safeExecute(view -> view.setAnnotationErrorVisibility(false));
                            getView().safeExecute(PdfReaderContract.View::closeReader);
                        },
                        throwable -> {
                            log.logError(throwable.getMessage());
                            getView().safeExecute(view -> view.setHasError(true));
                            getView().safeExecute(view -> view.setAnnotationErrorVisibility(true));
                            if (throwable instanceof IOException) {
                                getView().safeExecute(PdfReaderContract.View::errorAnnotUpdate);
                            }
                        }));
    }

    @Override
    public void registerAnnotation(String name, Map<String, String> annotations, String uuid, String xfdfCommand, String tagStart, String tagEnd) {
        interactor.registerAnnotation(name, annotations, uuid, xfdfCommand, tagStart, tagEnd);
    }

    @Override
    public String getChangesFromBKP(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands) {
        return interactor.createXFDFString(addCommands, modifyCommands, removeCommands);
    }

    @Override
    public Map<String, String> getChangeList(String name) {
        return interactor.getChangeList(name);
    }

    @Override
    public void discardAnnotationsBackup() {
        interactor.discarAnnotationsBackup();
    }

    @Override
    public void removeAnnotChangesFile() {
        interactor.removeAnnotChangesFile();
    }

    @Override
    public String readFileInPath(String filename){
        return interactor.readFile(filename);
    }

    @Override
    public String getAnnotationsChanges() {
        return interactor.readChangeFile();
    }
}
