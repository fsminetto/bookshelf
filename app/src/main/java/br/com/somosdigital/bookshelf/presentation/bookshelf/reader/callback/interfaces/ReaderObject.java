package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces;

/**
 * Created by pelisoli on 28/05/15.
 */
public interface ReaderObject {

        void onObjectCalled(String oedId, String objectPath);

        void onActivityCalled(String activityId, String activityPath, String activityType);

        void onErrorToOpenObject();

        void onOpenMedia(String mediaPath);
}
