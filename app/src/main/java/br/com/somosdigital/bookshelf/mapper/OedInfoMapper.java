package br.com.somosdigital.bookshelf.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;

/**
 * Created by pelisoli on 11/4/17.
 */

public class OedInfoMapper {

    public static List<OedInfo> mapToOedInfo(List<Oed> oedList){
        List<OedInfo> oedInfoList = new ArrayList<>();

        for (Oed oed : oedList){
            OedInfo newOedInfo = new OedInfo();
            newOedInfo.setId(oed.getId());
            newOedInfo.setPages(oed.getPages());
            newOedInfo.setName(oed.getTitle());
            newOedInfo.setLinkPreview(oed.getLinkPreview());

            oedInfoList.add(newOedInfo);
        }

        return oedInfoList;
    }

}
