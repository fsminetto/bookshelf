package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.fragments.activities_list;

import java.util.ArrayList;
import java.util.Collections;

import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;

/**
 * Created by marcus on 29/03/17.
 */

public class ActivitiesDialogPresenter extends BasePresenter<ActivitiesDialogContract.View> implements ActivitiesDialogContract.Presenter {

    @Override
    public void orderActivitiesByName(ArrayList<BookActivityInfo> activities) {

        Collections.sort(activities, (p1, p2) -> {
            int result = p1.getName().compareTo(p2.getName());

            if (result == 0) {
                result = p1.getId().compareTo(p2.getId());
            }
            return result;
        });

        getView().safeExecute(view -> view.showActivities(activities));
    }

}
