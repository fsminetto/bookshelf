package br.com.somosdigital.bookshelf.presentation.bookshelf.books.information;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by pelisoli on 12/4/17.
 */

public class BookInformationFragment
        extends Fragment implements BookInformationContract.View {

    public static final String EXTRA_BOOK_ID = "bookId";

    private String bookId;

    private BookInformationPresenter presenter;

    private TextView txvCollection;

    private TextView txvSubject;

    private TextView txvYear;

    private TextView txvSegment;

    private TextView txvAuthor;

    private TextView txvBookSize;

    private TextView txvOedSize;

    private ImageView imvBookCover;

    private TextView txvTitle;

    private LinearLayout viewContent;

    private ProgressBar loadingIcon;

    public static BookInformationFragment newInstance(String bookId) {
        BookInformationFragment bookInformationFragment = new BookInformationFragment();

        Bundle bundle = new Bundle();
        bundle.putString("bookId", bookId);

        bookInformationFragment.setArguments(bundle);

        return bookInformationFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        return inflater.inflate(R.layout.book_information, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViewsById(view);

        getBasicInfo(getArguments());

        presenter = new BookInformationPresenter();
        presenter.attachView(this);
        presenter.loadBook(bookId);

    }

    private void getBasicInfo(Bundle arguments) {
        if (arguments != null){
            bookId = arguments.getString(EXTRA_BOOK_ID);
        }
    }

    private void findViewsById(View view) {
        txvCollection = view.findViewById(R.id.book_information_txv_colection);
        txvSubject = view.findViewById(R.id.book_information_txv_subject);
        txvYear = view.findViewById(R.id.book_information_txv_year);
        txvSegment = view.findViewById(R.id.book_information_txv_segment);
        txvAuthor = view.findViewById(R.id.book_information_txv_authors);
        txvBookSize = view.findViewById(R.id.book_information_txv_book_size);
        txvOedSize = view.findViewById(R.id.book_information_txv_oeds_size);
        imvBookCover = view.findViewById(R.id.book_information_cover);
        txvTitle = view.findViewById(R.id.book_information_txv_title);
        viewContent = view.findViewById(R.id.content_layout);
        loadingIcon = view.findViewById(R.id.loading_view);
    }


    @Override
    public void showLoadingBookInfo(boolean show) {
        if(show) {
            loadingIcon.setVisibility(View.VISIBLE);
            viewContent.setVisibility(View.GONE);

        } else {
            loadingIcon.setVisibility(View.GONE);
            viewContent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showBook(Book book) {
        long bookAndActivitySize;
        String title = "";
        String collection = "";
        String discipline = "";
        String segment = "";
        String year = "";

        Glide.with(this)
                .load(book.getCoverPath())
                .placeholder(R.color.no_book)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(imvBookCover);


        if (book.getCollection() != null){
            collection = book.getCollection().getDescription();
        }

        if (book.getDisplayBookSubjects() != null) {
            discipline = book.getDisplayBookSubjects();
        }

        if (book.getGrade() != null) {
            year = book.getGrade().getDescription();
        }

        if (book.getSegment() != null){
            segment = book.getSegment().getDescription();
        }

        if (book.getTitle() != null) {
            title = book.getTitle();
        }

        txvTitle.setText(title);
        txvCollection.setText(collection);
        txvSubject.setText(discipline);
        txvSegment.setText(segment);
        txvYear.setText(year);

        txvOedSize.setText(presenter.getReadableFileSize(book.getOedLength()));

        bookAndActivitySize = book.getFileLength();
        bookAndActivitySize += book.getActivity() != null ? book.getActivity().getFileLength() : 0;

        txvBookSize.setText(presenter.getReadableFileSize(bookAndActivitySize));

        if (book.getAuthors() != null && book.getAuthors().size()>0) {
            String author = book.getAuthors().get(0).getName();
            txvAuthor.setText(author);
        }
    }

    @Override
    public void showServerMaintenanceError() {

    }
}