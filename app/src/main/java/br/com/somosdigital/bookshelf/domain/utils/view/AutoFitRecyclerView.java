package br.com.somosdigital.bookshelf.domain.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class AutoFitRecyclerView extends RecyclerView {

    private GridLayoutManager manager;
    private int columnWidth = -1;

    public AutoFitRecyclerView(Context context) {
        super(context);
        this.init(context, (AttributeSet)null);
    }

    public AutoFitRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context, attrs);
    }

    public AutoFitRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if(attrs != null) {
            int[] attrsArray = new int[]{16843031};
            TypedArray array = context.obtainStyledAttributes(attrs, attrsArray);
            this.columnWidth = array.getDimensionPixelSize(0, -1);
            array.recycle();
        }

        this.manager = new GridLayoutManager(this.getContext(), 1);
        this.setLayoutManager(this.manager);
    }

    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if(this.columnWidth > 0) {
            int spanCount = Math.max(1, this.getMeasuredWidth() / this.columnWidth);
            this.manager.setSpanCount(spanCount);
        }

    }
}
