package br.com.somosdigital.bookshelf.presentation.bookshelf.books.information;

import java.util.Locale;

import br.com.solucaoadapta.shared.presentation.base.BasePresenter;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.storage.module.BookshelfDatabase;
import br.com.somosdigital.bookshelf.storage.repository.book.BookRepository;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;
import io.realm.Realm;

/**
 * Created by michelribeiro on 01/11/17.
 */

public class BookInformationPresenter extends BasePresenter<BookInformationContract.View>
        implements BookInformationContract.Presenter {

    @Override
    public void loadBook(String bookId) {

        getView().safeExecute(view -> view.showLoadingBookInfo(true));

        Realm realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());

        BookSpecification bookSpecification = new BookRepository(realm);

        Book book = bookSpecification.getById(bookId);

        getView().safeExecute(view -> {
            view.showLoadingBookInfo(false);
            view.showBook(book);
        });

        realm.close();
    }

    @Override
    public String getReadableFileSize(long bytes){
        int exp;
        double megabytes = 1024 * 1024;

        if (bytes == 0){
            return "-";
        }

        if (bytes < megabytes) return bytes + " B";

        exp = (int) (Math.log(bytes) / Math.log(1000));
        String pre = ("kMGTPE").charAt(exp-1) + ("");

        return String.format(Locale.ENGLISH,"%.1f %sB", bytes / megabytes, pre);

    }
}
