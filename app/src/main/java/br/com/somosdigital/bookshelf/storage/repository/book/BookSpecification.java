package br.com.somosdigital.bookshelf.storage.repository.book;


import java.util.List;

import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface BookSpecification extends Repository<Book> {
    List<Book> getBooks();

    List<Book> getDownloadedBooks();

    Book getById(String idBook);

    void closeRealm();

    void openRealm();
}
