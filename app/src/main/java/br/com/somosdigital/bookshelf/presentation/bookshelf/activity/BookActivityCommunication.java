package br.com.somosdigital.bookshelf.presentation.bookshelf.activity;

/**
 * Created by pelisoli on 9/26/17.
 */

public interface BookActivityCommunication {

    void closeParentActivity();

    void selectStudents();

}
