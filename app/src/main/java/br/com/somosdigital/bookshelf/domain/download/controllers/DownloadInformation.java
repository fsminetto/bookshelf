package br.com.somosdigital.bookshelf.domain.download.controllers;

import java.io.File;

import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import io.realm.RealmList;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadInformation {

    private static String currentBookId = "";

    private static int currentDownloadId = 0;

    private static RealmList<Book> downloadQueue = new RealmList<>();

    public static float getProgress(Book book) {
        if (book.getTotalBytesToDownload() > 0) {
            float value = ((book.getCurrentBytesDownloaded() * 100) / book.getTotalBytesToDownload()) / 100;

            if (value > 0.99f) {
                return 0.99f;
            } else {
                return value;
            }

        } else {
            return 0;
        }
    }

    public static String getCurrentBookId() {
        return currentBookId;
    }

    public static void setCurrentBookId(String bookId) {
        currentBookId = bookId;
    }

    public static int getCurrentDownloadId() {
        return currentDownloadId;
    }

    public static void setCurrentDownloadId(int currentDownloadId) {
        DownloadInformation.currentDownloadId = currentDownloadId;
    }

    public static void setDownloadQueue(RealmList<Book> downloadQueue) {
        DownloadInformation.downloadQueue = downloadQueue;
    }

    public static RealmList<Book> getDownloadQueue() {
        return downloadQueue;
    }

    public static void addDownloadQueue(Book book) {
        downloadQueue.add(book);
    }

    public static void calculateDownloadSize(Book book, int downloadType) {

        float totalBookSize = book.getFileLength();
        float totalActivitySize = book.getActivity() != null ? book.getActivity().getFileLength() : 0;
        float totalOedSize = calculateTotalOedSize(book);

        if (downloadType == DownloadController.BOOK_COMPLETE) {
            book.setCurrentBytesDownloaded(calculateAlreadyDownloadedBytes(book, true));
            book.setTotalBytesToDownload(totalBookSize + totalActivitySize + totalOedSize);
        } else if (downloadType == DownloadController.BOOK_ACTIVITY) {
            book.setCurrentBytesDownloaded(calculateAlreadyDownloadedBytes(book, false));
            book.setTotalBytesToDownload(totalBookSize + totalActivitySize);
        } else if (downloadType == DownloadController.BOOK_OED) {
            book.setCurrentBytesDownloaded(calculateAlreadyDownloadedOedSize(book));
            book.setTotalBytesToDownload(totalOedSize);
        }
    }

    private static float calculateAlreadyDownloadedBytes(Book book, boolean includeOed){
        float totalAlreadyDownloaded = calculateBookAlreadyDownloadedSize(book) +
                calculateActivityAlreadyDownloadedSize(book);

        if (includeOed){
            totalAlreadyDownloaded += calculateAlreadyDownloadedOedSize(book);
        }

        return totalAlreadyDownloaded;
    }

    private static float calculateActivityAlreadyDownloadedSize(Book book){
        float activityDownloaded = 0;

        if (book.getActivity() != null){
            if (book.getActivity().getDownloadStatus() == DownloadStatus.SUCCESSFUL){
                activityDownloaded += book.getActivity().getFileLength();
            } else {
                if (book.getActivity().getCompactedPath() != null){
                    File activityFile = new File(book.getActivity().getCompactedPath());
                    if (activityFile.exists()){
                        activityDownloaded += activityFile.length();
                    }
                }
            }
        }

        return activityDownloaded;
    }

    private static float calculateBookAlreadyDownloadedSize(Book book){
        float bookDownloaded = 0;
        if (book.getDownloadStatus() == DownloadStatus.SUCCESSFUL ||
                book.getDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY ||
                book.getDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_OED){
            bookDownloaded += book.getFileLength();
        } else {
            if (book.getCompactedPath() != null){
                File bookFile = new File(book.getCompactedPath());
                if (bookFile.exists()){
                    bookDownloaded += bookFile.length();
                }
            }
        }
        return bookDownloaded;
    }

    private static float calculateAlreadyDownloadedOedSize(Book book) {
        float alreadyDownloaded = 0;
        if (book.getEducationalObjects() != null){
            for (Oed oed : book.getEducationalObjects()){
                if (oed.getDownloadStatus() == DownloadStatus.SUCCESSFUL || oed.getDownloadStatus() == DownloadStatus.DECOMPRESSING){
                    alreadyDownloaded += oed.getFileLength();
                } else {
                    if (oed.getCompactedPath() != null) {
                        File file = new File(oed.getCompactedPath());
                        if (file.exists()) {
                            alreadyDownloaded += file.length();
                        }
                    }
                }
            }
        }
        return alreadyDownloaded;
    }

    private static float calculateTotalOedSize(Book book) {
        float totalSize = 0;
        if (book.getEducationalObjects() != null && book.getEducationalObjects().size() > 0) {
            for (Oed oed : book.getEducationalObjects()) {
                totalSize += oed.getFileLength();
            }
        }
        return totalSize;
    }
}
