package br.com.somosdigital.bookshelf.service.distribuidor;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.solucaoadapta.shared.domain.openid.TokenManager;
import br.com.solucaoadapta.shared.domain.services.AuthenticatorManager;
import br.com.somosdigital.bookshelf.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by marcus.martins on 05/09/16.
 */
public class DistribuidorServiceGenerator {

    public static DistribuidorServiceInterface createService(final TokenManager tokenManager, String apiBaseUrl) {

        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .setPrettyPrinting()
                .create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        builder.baseUrl(apiBaseUrl);

        /** O logging interceptor causa crash no rxjava devido algumas requisicoes voltarem com response
         null, portanto deve ser tratadas ou simplesmente não deixá-lo ativo **/
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(message -> Timber.tag("OkHttp").d(message));
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingInterceptor);

        Retrofit retrofit = builder.client(
                new AuthenticatorManager(httpClient, tokenManager).authenticate().build()).build();
        return retrofit.create(DistribuidorServiceInterface.class);

    }

    public static DistribuidorServiceInterface createService(final TokenManager tokenManager) {
        return createService(tokenManager, BuildConfig.APS_BOOKSHELF_URL);
    }
}
