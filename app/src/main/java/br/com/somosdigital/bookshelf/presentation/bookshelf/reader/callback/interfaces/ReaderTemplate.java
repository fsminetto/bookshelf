package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces;

import org.json.JSONException;

/**
 * Created by marcus on 31/10/16.
 */

public interface ReaderTemplate {
    String getClassInfo();

    void sendActivity(String activity, String statement);

    String getAllActivityResults(String url) throws JSONException;

    String saveActivity(String statements);

    String saveActivityKeepOpen(String statements);

    void closeActivity();
}
