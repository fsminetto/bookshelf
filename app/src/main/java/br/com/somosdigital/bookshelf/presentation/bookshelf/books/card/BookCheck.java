package br.com.somosdigital.bookshelf.presentation.bookshelf.books.card;

import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by marcus on 26/04/17.
 */

public class BookCheck {

    public static boolean containsOED(Book book) {
        return book.getOedLength() != 0;
    }

    public static boolean isBookActive(Book book) {
        return (!book.isExpiredDistribution()) && ((book.getActivity() != null
                && book.getActivity().getDownloadStatus() == DownloadStatus.SUCCESSFUL)
                || (book.getActivity() == null
                && book.getDownloadStatus() == DownloadStatus.SUCCESSFUL));
    }

    public static boolean isShowProgress(Book book) {
        return !(book.getGlobalDownloadStatus() == DownloadStatus.NEW
                || book.getGlobalDownloadStatus() == DownloadStatus.FAILED
                || book.isInQueue()
                || book.getGlobalDownloadStatus() == DownloadStatus.UPDATE
                || book.getGlobalDownloadStatus() == DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL
                || book.getGlobalDownloadStatus() == DownloadStatus.ALL_BOOK_SUCCESSFUL);
    }

}
