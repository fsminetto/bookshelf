package br.com.somosdigital.bookshelf.presentation.bookshelf.oed;

import br.com.solucaoadapta.shared.presentation.base.MvpPresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.callback.interfaces.ReaderObject;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.utils.OedManagerAndroid;

/**
 * Created by marcus on 01/11/16.
 */

interface OedContract {
    interface View extends MvpView {
        void onOedReadyLocally(String oedPath, String key);

        void loadOedOnline();

        void openMedia(String mediaPath);
    }

    interface Presenter extends MvpPresenter<View> {
        void loadOed(String objectPath, String oedId, String bookId, OedManagerAndroid oedManagerAndroid);

        ReaderObject getObjectCallback();
    }
}