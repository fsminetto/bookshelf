package br.com.somosdigital.bookshelf.storage.module;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Collection;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Grade;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Segment;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Subject;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import io.realm.annotations.RealmModule;

/**
 * Created by pelisoli on 8/30/17.
 */

@RealmModule(
        library = true,
        classes = {Book.class, Collection.class, Grade.class, Subject.class,
                Template.class, BookshelfUser.class, Segment.class, Oed.class, Activity.class})
public class BookshelfModule {
}
