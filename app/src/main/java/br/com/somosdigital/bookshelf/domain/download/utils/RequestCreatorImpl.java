package br.com.somosdigital.bookshelf.domain.download.utils;

import android.net.Uri;

import java.io.File;

import br.com.converge.download.android.DefaultRetryPolicy;
import br.com.converge.download.android.DownloadRequest;
import br.com.converge.download.android.DownloadStatusListener;
import br.com.converge.download.android.ThinDownloadManager;

/**
 * Created by pelisoli on 01/06/16.
 */
public class RequestCreatorImpl implements RequestCreator {
    private ThinDownloadManager downloadManager;

    public RequestCreatorImpl() {
    }

    public RequestCreatorImpl(ThinDownloadManager downloadManager) {
        this.downloadManager = downloadManager;
    }

    @Override
    public ThinDownloadManager getDownloadManager() {
        return downloadManager;
    }

    @Override
    public void setDownloadManager(ThinDownloadManager downloadManager){
        this.downloadManager = downloadManager;
    }

    @Override
    public DownloadRequest createDownloadRequest(String downloadPath,
                                                 String destinationPath,
                                                 DownloadStatusListener listener) {
        long fileSize = 0;
        Uri downloadUri = Uri.parse(downloadPath);
        Uri destinationUri = Uri.parse(destinationPath);
        File file = new File(destinationPath);
        DownloadRequest downloadRequest;

        if (file.exists()) {
            fileSize = file.length();

            downloadRequest = new DownloadRequest(downloadUri)
                    .addCustomHeader("Range", "bytes=" + fileSize + "-")
                    .setRetryPolicy(new DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(listener);

        } else {
            downloadRequest = new DownloadRequest(downloadUri)
                    .setRetryPolicy(new DefaultRetryPolicy())
                    .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                    .setDownloadListener(listener);
        }

        return downloadRequest;
    }

    @Override
    public DownloadRequest createNewDownloadRequest(String downloadPath,
                                                    String destinationPath,
                                                    DownloadStatusListener listener) {
        Uri downloadUri = Uri.parse(downloadPath);
        Uri destinationUri = Uri.parse(destinationPath);
        DownloadRequest downloadRequest;

        downloadRequest = new DownloadRequest(downloadUri)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadListener(listener);

        return downloadRequest;
    }

    @Override
    public DownloadRequest createEtagRequest(String downloadPath,
                                             String destinationPath,
                                             String eTag,
                                             DownloadStatusListener listener) {
        Uri downloadUri = Uri.parse(downloadPath);
        Uri destinationUri = Uri.parse(destinationPath);
        DownloadRequest downloadRequest;

        downloadRequest = new DownloadRequest(downloadUri)
                .addCustomHeader("ETag", eTag)
                .setRetryPolicy(new DefaultRetryPolicy())
                .setDestinationURI(destinationUri).setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadListener(listener);

        return downloadRequest;
    }

}
