package br.com.somosdigital.bookshelf.presentation.bookshelf.books.card;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pnikosis.materialishprogress.ProgressWheel;

import br.com.somosdigital.bookshelf.R;


/**
 * Created by marcus on 20/09/16.
 */

public class CardViewHolder extends RecyclerView.ViewHolder implements ProgressAction {

    public ImageView imageBookCover;

    public ImageView imageBookAlpha;

    public ImageView downloadIcon;

    public TextView bookPrimaryInfo;

    public TextView bookSecondaryInfo;

    public ProgressWheel downloadProgress;

    public ImageView actionMenu;

    public RelativeLayout rlBottomInformation;

    public ImageView imvNoActivity;

    public TextView txtExpiration;

    public RelativeLayout expirationLayout;

    public TextView teacherBookText;

    public CardViewHolder(View itemView) {
        super(itemView);
        actionMenu = itemView.findViewById(R.id.image_action_overflow);
        downloadProgress = itemView.findViewById(R.id.downloadProgressBar);
        bookSecondaryInfo = itemView.findViewById(R.id.book_secondary_info_txt);
        bookPrimaryInfo = itemView.findViewById(R.id.book_primary_info_txt);
        downloadIcon = itemView.findViewById(R.id.image_bookcell_download_icon);
        imageBookCover = itemView.findViewById(R.id.image_bookcell_cover);
        imageBookAlpha = itemView.findViewById(R.id.image_bookcell_alpha);
        rlBottomInformation = itemView.findViewById(R.id.rl_book_card_bottom_information);
        imvNoActivity = itemView.findViewById(R.id.book_card_imv_no_activity);
        txtExpiration = itemView.findViewById(R.id.txtExpiration);
        expirationLayout = itemView.findViewById(R.id.rl_expiration);
        teacherBookText = itemView.findViewById(R.id.txt_teacher_book);
    }

    @Override
    public void onProgressUpdate(float progress) {
        if (progress <= 0.99f) {
            downloadProgress.setLinearProgress(true);
            downloadProgress.setProgress(progress);
        } else {
            downloadProgress.setProgress(0.99f);
        }
        downloadProgress.invalidate();
    }

    @Override
    public void spinProgress() {
        downloadProgress.spin();
    }
}