package br.com.somosdigital.bookshelf.presentation.bookshelf.books.menu;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.View;

import br.com.solucaoadapta.shared.storage.runtime.AppRunningMode;
import br.com.solucaoadapta.shared.storage.runtime.ClassStatusManager;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.BookCheck;


/**
 * Created by jgsmanaroulas on 18/03/16.
 */
public class BookPopupMenu {

    public static PopupMenu createMenu(Context context, View parent, Book book) {
        PopupMenu popupMenu = new PopupMenu(context, parent);
        popupMenu.inflate(R.menu.popup_menu);


        if (ClassStatusManager.getInstance().getAppRunningMode() == AppRunningMode.OFFLINE) {
            popupMenu.getMenu().findItem(R.id.update_book).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_book).setVisible(false);
            popupMenu.getMenu().findItem(R.id.pause_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.continue_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.cancel_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.repair_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.delete_book).setVisible(false);
            popupMenu.getMenu().findItem(R.id.remove_book_queue).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_complete).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_oeds).setVisible(false);
            popupMenu.getMenu().findItem(R.id.delete_oeds).setVisible(false);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

            return popupMenu;
        }

        if (book.isInQueue()) {
            popupMenu.getMenu().findItem(R.id.remove_book_queue).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.NEW) {

            popupMenu.getMenu().findItem(R.id.start_download_book).setVisible(true);
            popupMenu.getMenu().findItem(R.id.start_download_complete).setVisible(BookCheck.containsOED(book));
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY ||
                book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_OED ||
                book.getGlobalDownloadStatus() == DownloadStatus.DECOMPRESSING) {

            popupMenu.getMenu().findItem(R.id.pause_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.cancel_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.PAUSED) {

            popupMenu.getMenu().findItem(R.id.continue_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.cancel_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.FAILED) {

            popupMenu.getMenu().findItem(R.id.repair_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.cancel_download).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.UPDATE) {

            popupMenu.getMenu().findItem(R.id.update_book).setVisible(true);
            popupMenu.getMenu().findItem(R.id.delete_book).setVisible(true);
            popupMenu.getMenu().findItem(R.id.delete_oeds).setVisible(BookCheck.containsOED(book));
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL) {

            popupMenu.getMenu().findItem(R.id.start_download_oeds).setVisible(BookCheck.containsOED(book));
            popupMenu.getMenu().findItem(R.id.delete_book).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);

        } else if (book.getGlobalDownloadStatus() == DownloadStatus.ALL_BOOK_SUCCESSFUL) {
            popupMenu.getMenu().findItem(R.id.delete_oeds).setVisible(BookCheck.containsOED(book));
            popupMenu.getMenu().findItem(R.id.delete_book).setVisible(true);
            popupMenu.getMenu().findItem(R.id.book_information).setVisible(true);
        }

        if(book.isExpiredDistribution()) {
            popupMenu.getMenu().findItem(R.id.update_book).setVisible(false);
            popupMenu.getMenu().findItem(R.id.pause_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.cancel_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_oeds).setVisible(false);
            popupMenu.getMenu().findItem(R.id.continue_download).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_book).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_complete).setVisible(false);
            popupMenu.getMenu().findItem(R.id.repair_download).setVisible(false);
        }

        if(!book.isAllowOedDownload()){
            popupMenu.getMenu().findItem(R.id.start_download_oeds).setVisible(false);
            popupMenu.getMenu().findItem(R.id.delete_oeds).setVisible(false);
            popupMenu.getMenu().findItem(R.id.start_download_complete).setVisible(false);
        }

        return popupMenu;
    }
}