package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron;

import java.util.ArrayList;

import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.storage.preferences.LocalPreferences;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;

/**
 * Created by marcus on 01/12/16.
 */

public interface PdfReaderCommunication {

    void startOed(String objectPath, String bookId, OedInfo oedInfo);

    void startBookActivity(BookActivityInfo bookActivityInfo, String bookId, boolean authorship);

    LocalPreferences getApplicationLocalPreferences();

    Log getLog();

    void sendActivities(ArrayList<BookActivityInfo> mActivities);

    /**
     * A implementação deste método deverá conter a lógica (se houver) para fechar e sair do leitor.
     * a fim de preparar o leitor para ser fechado, deverá ser chamado o método willLeave()
     * no pdfReaderFragment(),  que por sua vez, invocará este método quando pronto.
     */
    void closeReader();
}
