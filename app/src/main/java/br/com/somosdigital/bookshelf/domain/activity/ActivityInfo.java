package br.com.somosdigital.bookshelf.domain.activity;

/**
 * Created by pelisoli on 06/12/16.
 */

public class ActivityInfo {

    private String bookId;

    private String activityType;

    private String activityId;

    private boolean autoria;

    private String contentId;

    private String bookPage;

    private String sentActivityStudentId;

    private boolean answerBlocked;

    public ActivityInfo(String bookId, String activityType, String activityId, String contentId, boolean autoria,
                        String sentActivityStudentId, boolean answerBlocked) {
        this.bookId = bookId;
        this.activityType = activityType;
        this.activityId = activityId;
        this.contentId = contentId;
        this.autoria = autoria;
        this.sentActivityStudentId = sentActivityStudentId;
        this.answerBlocked = answerBlocked;
    }

    public ActivityInfo(String bookId, String activityType, String activityId, String contentId, boolean autoria,
                        String bookPage) {
        this.bookId = bookId;
        this.activityType = activityType;
        this.activityId = activityId;
        this.contentId = contentId;
        this.autoria = autoria;
        this.bookPage = bookPage;
    }

    public ActivityInfo(String bookId, String activityType, String activityId, String contentId, boolean autoria,
                        String bookPage, String sentActivityStudentId, boolean answerBlocked) {
        this.bookId = bookId;
        this.activityType = activityType;
        this.activityId = activityId;
        this.contentId = contentId;
        this.autoria = autoria;
        this.bookPage = bookPage;
        this.sentActivityStudentId = sentActivityStudentId;
        this.answerBlocked = answerBlocked;
    }

    public String getBookId() {
        return bookId;
    }

    public String getActivityType() {
        return activityType;
    }

    public String getActivityId() {
        return activityId;
    }

    public boolean isAutoria() {
        return autoria;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public void setAutoria(boolean autoria) {
        this.autoria = autoria;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getBookPage() {
        return bookPage;
    }

    public void setBookPage(String bookPage) {
        this.bookPage = bookPage;
    }

    public String getSentActivityStudentId() {
        return sentActivityStudentId;
    }

    public boolean isAnswerBlocked() {
        return answerBlocked;
    }
}
