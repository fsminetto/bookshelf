package br.com.somosdigital.bookshelf.domain.download.utils;

import android.content.Context;

import java.io.File;

import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.cryptography.CryptographyManager;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Activity;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import io.reactivex.Observable;

/**
 * Created by marcus on 17/04/17.
 */
public class DownloadUnzipImpl implements DownloadUnzip {

    private Log log;

    private Context context;

    private String preferenceName;

    public DownloadUnzipImpl(Log log, Context context, String preferenceName) {
        this.log = log;
        this.context = context;
        this.preferenceName = preferenceName;
    }

    @Override
    public Observable<Boolean> unzipBook(Book book) {
        return Observable.defer(() -> {
            log.logInfo("Unzipping Book");

            if(new File(book.getTempPath()).exists()) {
                FileManager.removeDirectory(book.getTempPath());
            }

            try {
                FileManager.unzip(book.getCompactedPath(), book.getTempPath());
            } catch (Exception e) {
                return Observable.error(e);
            }

            if (new File(book.getCompactedPath()).delete()) {
                log.logInfo("Delete BookCompactedPath Success");
            } else {
                log.logError("Delete BookCompactedPath Error");
            }

            File finalBookPath = new File(book.getBookPath());
            new File(book.getTempPath()).renameTo(finalBookPath);

            CryptographyManager.createKeyCriptographyFile(book.getBrkPath(),
                    book.getKeyCryptography());

            return Observable.just(true);

        });
    }

    @Override
    public Observable<Boolean> unzipActivity(Activity activity) {
        return Observable.defer(() -> {

            log.logInfo("Unzipping Activity");

            if(new File(activity.getTempPath()).exists()) {
                FileManager.removeDirectory(activity.getTempPath());
            }

            //decrypting the password
            String password = CryptographyManager
                    .decriptyFromKey(activity.getPassword(), preferenceName, context);

            try {
                FileManager.unzip(activity.getCompactedPath(), activity.getTempPath(), password);
            } catch (Exception e) {
                return Observable.error(e);
            }

            if (new File(activity.getCompactedPath()).delete()) {
                log.logInfo("Delete ActivityCompactedPath Success");
            } else {
                log.logError("Delete ActivityCompactedPath Error");
            }

            File finalActivityPath = new File(activity.getActivityPath());
            new File(activity.getTempPath()).renameTo(finalActivityPath);

            CryptographyManager.createKeyCriptographyFile(activity.getBrkPath(),
                    activity.getKeyCryptography());

            return Observable.just(true);

        });

    }

    @Override
    public Observable<Boolean> unzipOed(Oed oed) {

        return Observable.defer(() -> {
            log.logInfo("Unzipping Oed");

            if(new File(oed.getTempPath()).exists()) {
                FileManager.removeDirectory(oed.getTempPath());
            }

            //decrypting the password
            String password = CryptographyManager
                    .decriptyFromKey(oed.getPassword(), preferenceName, context);

            try {
                FileManager.unzip(oed.getCompactedPath(), oed.getTempPath(), password);
            } catch (Exception e) {
                return Observable.error(e);
            }

            if (new File(oed.getCompactedPath()).delete()) {
                log.logInfo("Delete OedCompactedPath Success");
            } else {
                log.logError("Delete OedCompactedPath Error");
            }

            File finalOedPath = new File(oed.getOedPath());
            new File(oed.getTempPath()).renameTo(finalOedPath);

            CryptographyManager.createKeyCriptographyFile(oed.getBrkPath(),
                    oed.getKeyCryptography());

            return Observable.just(true);
        });
    }

    @Override
    public Observable<Boolean> unzipTemplate(Template template) {
        return Observable.defer(() -> {
            log.logInfo("Unzipping Template");

            if(template != null) {

                if (new File(template.getTempPath()).exists()) {
                    FileManager.removeDirectory(template.getTempPath());
                }

                try {
                    FileManager.unzip(template.getZipPath(), template.getTempPath());
                } catch (Exception e) {
                    return Observable.error(e);
                }

                if (new File(template.getZipPath()).delete()) {
                    log.logInfo("Success on deleting template zip file");
                } else {
                    log.logError("Error on deleting template zip file");
                }

                File finalBookPath = new File(template.getDestPath());
                File oldTemplate = new File(finalBookPath.getParent(), "oldTemplate" );

                if(new File(template.getDestPath()).exists()) {
                    if (new File(template.getDestPath()).renameTo(oldTemplate)) {
                        log.logInfo("Successful in renaming template folder to old template folder");
                    } else {
                        log.logInfo("Error in renaming template folder to old template folder");
                    }
                }

                if(new File(template.getTempPath()).renameTo(finalBookPath)){
                    log.logInfo("Successful in renaming temp template folder to final template folder");
                }else{
                    log.logInfo("Error in renaming temp template folder to final template folder");
                }

                FileManager.removeDirectory(oldTemplate.getPath());

                return Observable.just(true);
            }else {
                return Observable.just(false);
            }
        });
    }
}
