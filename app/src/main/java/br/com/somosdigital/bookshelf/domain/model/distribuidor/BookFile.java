package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;

import io.realm.RealmList;

/**
 * Created by jgsmanaroulas on 16/02/17.
 */
public class BookFile {

    @Expose()
    private String bookId;

    @Expose()
    private String fileType;

    @Expose()
    private  String keyCryptography;

    @Expose(serialize = false)
    private long fileLength;

    @Expose(serialize = false)
    private String filePath;

    @Expose(serialize = false)
    private double version;

    @Expose(serialize = false)
    private RealmList<Oed> educationalObjects;

    @Expose(serialize = false)
    private Activity activity;

    @Expose()
    private Device device;

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public RealmList<Oed> getEducationalObjects() {
        return educationalObjects;
    }

    public void setEducationalObjects(RealmList<Oed> educationalObjects) {
        this.educationalObjects = educationalObjects;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getKeyCryptography() {
        return keyCryptography;
    }

    public void setKeyCryptography(String keyCryptography) {
        this.keyCryptography = keyCryptography;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
