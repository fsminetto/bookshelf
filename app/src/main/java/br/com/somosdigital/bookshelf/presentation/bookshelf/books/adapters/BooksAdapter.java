package br.com.somosdigital.bookshelf.presentation.bookshelf.books.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.com.solucaoadapta.shared.domain.utils.ISO8601DateFormat;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.BooksFragmentContract;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.BookCheck;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.CardViewHolder;

/**
 * Created by marcus.martins on 20/03/16.
 */
public class BooksAdapter extends RecyclerView.Adapter<CardViewHolder> {

    private final boolean showNoActivityIcon;

    private List<Book> bookList;

    private Context context;

    private BooksFragmentContract.View booksView;

    private static DateFormat formatterAPI = new ISO8601DateFormat();

    private static SimpleDateFormat formatterAPP = new SimpleDateFormat("dd/MM/yyyy");

    public BooksAdapter(Context context,
                        List<Book> bookList,
                        BooksFragmentContract.View booksView, boolean showNoActivityIcon) {
        this.context = context;
        this.bookList = bookList;
        this.booksView = booksView;
        this.showNoActivityIcon = showNoActivityIcon;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.book_card, parent, false);
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardViewHolder cardViewHolder, int position) {
        if (bookList != null) {
            Book book = bookList.get(position);

            if (book != null) {
                book.setBookPosition(position);
                book.setCardProgress(cardViewHolder);

                String primaryInfo;
                if (book.getPrimaryInfo() != null && !book.getPrimaryInfo().isEmpty()) {
                    primaryInfo = book.getPrimaryInfo();
                } else {
                    primaryInfo = book.getCollection() != null ? book.getCollection().getName() : "";
                }

                String secondaryInfo;
                if (book.getSecondaryInfo() != null && !book.getSecondaryInfo().isEmpty()) {
                    secondaryInfo = book.getSecondaryInfo();
                } else {
                    if (book.getSubjectList() != null && book.getSubjectList().size() > 1) {
                        secondaryInfo = context.getString(R.string.multi_subjects);
                    } else {
                        secondaryInfo = book.getDisplayBookTitle();
                    }
                }

                if (book.isTeacherBook()){
                    cardViewHolder.teacherBookText.setVisibility(View.VISIBLE);
                } else {
                    cardViewHolder.teacherBookText.setVisibility(View.GONE);
                }

                cardViewHolder.bookPrimaryInfo.setText(primaryInfo);
                cardViewHolder.bookSecondaryInfo.setText(secondaryInfo);

                if (!book.isInQueue()) {
                    cardViewHolder.downloadIcon.setImageResource(DownloadStatus
                            .fromInt(book.getGlobalDownloadStatus().getId()).getImage());
                } else {
                    cardViewHolder.downloadIcon.setImageResource(R.drawable.ic_sm_livro_fila);
                }

                if (book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_ACTIVITY ||
                        book.getGlobalDownloadStatus() == DownloadStatus.DOWNLOADING_BOOK_OED ||
                        book.getGlobalDownloadStatus() == DownloadStatus.DECOMPRESSING) {
                    cardViewHolder.downloadIcon.setColorFilter(ContextCompat.getColor(context, R.color.disabled_gray));
                } else {
                    cardViewHolder.downloadIcon.setColorFilter(ContextCompat.getColor(context, R.color.plurall_blue_gray));
                }

                if (!book.isInQueue() && book.getGlobalDownloadStatus() == DownloadStatus.FAILED) {
                    cardViewHolder.downloadIcon.setColorFilter(ContextCompat.getColor(context, R.color.bookshelf_error));
                }


                if (book.getActivity() == null && showNoActivityIcon) {
                    cardViewHolder.imvNoActivity.setVisibility(View.VISIBLE);
                } else {
                    cardViewHolder.imvNoActivity.setVisibility(View.GONE);
                }

                if (book.isExpiredDistribution()) {
                    cardViewHolder.expirationLayout.setVisibility(View.VISIBLE);
                    cardViewHolder.imageBookAlpha.setVisibility(View.VISIBLE);
                    cardViewHolder.txtExpiration.setVisibility(View.VISIBLE);
                    cardViewHolder.txtExpiration.setText(context.getString(R.string.expired));
                } else {
                    if (BookCheck.isBookActive(book)) {
                        cardViewHolder.imageBookAlpha.setVisibility(View.INVISIBLE);
                    } else {
                        cardViewHolder.imageBookAlpha.setVisibility(View.VISIBLE);
                    }

                    if (book.isExpires()) {
                        cardViewHolder.expirationLayout.setVisibility(View.VISIBLE);
                        cardViewHolder.txtExpiration.setVisibility(View.VISIBLE);
                        cardViewHolder.txtExpiration.setText(context.getString(R.string.expires_in, formattedDate(book.getEndDateDistribution())));
                    } else {
                        cardViewHolder.expirationLayout.setVisibility(View.GONE);
                    }
                }

                cardViewHolder.actionMenu.setOnClickListener(view ->
                        booksView.showBookMenu(cardViewHolder, book));

                cardViewHolder.rlBottomInformation.setOnClickListener(view -> {
                    if (BookCheck.isBookActive(book)) {
                        booksView.openBook(book);
                    }
                });

                cardViewHolder.imageBookCover.setOnClickListener(view -> {
                    if (BookCheck.isBookActive(book)) {
                        booksView.openBook(book);
                    }
                });

                if (BookCheck.isShowProgress(book)) {
                    cardViewHolder.downloadProgress.setVisibility(View.VISIBLE);
                    cardViewHolder.downloadProgress.setLinearProgress(true);
                    DownloadInformation.calculateDownloadSize(book, book.getDownloadType());
                    cardViewHolder.downloadProgress.setInstantProgress(DownloadInformation.getProgress(book));
                } else {
                    cardViewHolder.downloadProgress.setVisibility(View.INVISIBLE);
                }

                if (book.getGlobalDownloadStatus() == DownloadStatus.DECOMPRESSING
                        || book.getGlobalDownloadStatus() == DownloadStatus.CANCELING
                        || book.getGlobalDownloadStatus() == DownloadStatus.DELETING) {
                    cardViewHolder.downloadProgress.spin();
                }

                Glide.with(context)
                        .load(book.getCoverPath())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .crossFade()
                        .placeholder(R.color.no_book)
                        .into(cardViewHolder.imageBookCover);

            }
        }
    }

    @Override
    public int getItemCount() {
        return bookList != null ? bookList.size() : 0;
    }

    public void addNewBooks(List<Book> newBookList) {
        if (newBookList != null) {
            bookList = new ArrayList<>();
            bookList.addAll(newBookList);
            notifyDataSetChanged();
        }
    }

    private static String formattedDate(String strDate) {
        Date date;

        formatterAPP.setTimeZone(TimeZone.getDefault());
        formatterAPI.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            date = formatterAPI.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
            return strDate;
        }

        return formatterAPP.format(date);
    }
}
