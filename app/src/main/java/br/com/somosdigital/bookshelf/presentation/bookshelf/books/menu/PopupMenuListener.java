package br.com.somosdigital.bookshelf.presentation.bookshelf.books.menu;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;

import br.com.solucaoadapta.shared.presentation.dialog.choice.ChoiceDialog;
import br.com.solucaoadapta.shared.presentation.dialog.choice.ChoiceDialogCallback;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.presentation.bookshelf.BookshelfActivityCommunication;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.DownloadActions;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.information.BookInformationDialog;

/**
 * Created by marcus on 08/10/16.
 */
public class PopupMenuListener implements PopupMenu.OnMenuItemClickListener {
    private Context context;

    private Book book;

    private DownloadActions downloadActions;

    private BookshelfActivityCommunication communication;

    public PopupMenuListener(Context context,
                             DownloadActions downloadActions,
                             Book book,
                             BookshelfActivityCommunication communication) {
        this.context = context;
        this.downloadActions = downloadActions;
        this.book = book;
        this.communication = communication;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int itemId = menuItem.getItemId();

        if (itemId == R.id.start_download_book) {
            downloadActions.startDownloadBook(book, true, false);
            return true;

        } else if (itemId == R.id.start_download_complete) {
            downloadActions.startDownloadBook(book, true, true);
            return true;

        } else if (itemId == R.id.start_download_oeds){
            downloadActions.startDownloadOeds(book, true);
            return true;

        } else if (itemId == R.id.continue_download) {
            downloadActions.continueDownload(book, true);
            return true;

        } else if (itemId == R.id.repair_download) {
            downloadActions.continueDownload(book, true);
            return true;

        } else if (itemId == R.id.update_book) {
            downloadActions.updateBook(book);
            return true;
        } else if (itemId == R.id.pause_download) {
            downloadActions.pauseDownload(book);
            return true;
        } else if (itemId == R.id.cancel_download) {
            askCancelDownload(downloadActions);
            return true;
        } else if (itemId == R.id.delete_book) {
            askDeleteBook(downloadActions);
            return true;
        } else if (itemId == R.id.remove_book_queue) {
            askRemoveQueue(downloadActions);
            return true;
        } else if (itemId == R.id.book_information){
            boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);

            if (tabletSize) {
                BookInformationDialog frag = BookInformationDialog.newInstance(book.getId());
                frag.show(((AppCompatActivity)context).getSupportFragmentManager(), null);
            } else {
                communication.showBookInformation(book.getId());
            }

            return true;
        } else if (itemId == R.id.delete_oeds){
            askDeleteOeds(downloadActions);
            return true;
        }

        return false;
    }

    private void askDeleteOeds(DownloadActions downloadActions) {
        ChoiceDialog dialog = ChoiceDialog.newInstance(context.getString(R.string.confirm_delete_oeds));
        dialog.setCallback(new ChoiceDialogCallback() {
            @Override
            public void onCanceled() {

            }
            @Override
            public void onAccepted() {
                downloadActions.deleteBookOeds(book);
            }
        });
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        ft.add(dialog, "ChoiceDialog");
        ft.commitAllowingStateLoss();
    }

    private void askDeleteBook(DownloadActions downloadActions) {
        ChoiceDialog dialog = ChoiceDialog.newInstance(context.getString(R.string.confirm_delete_book));
        dialog.setCallback(new ChoiceDialogCallback() {
            @Override
            public void onCanceled() {

            }

            @Override
            public void onAccepted() {
                downloadActions.deleteBook(book);
            }
        });
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        ft.add(dialog, "ChoiceDialog");
        ft.commitAllowingStateLoss();
    }

    private void askCancelDownload(DownloadActions downloadActions) {
        ChoiceDialog dialog = ChoiceDialog.newInstance(context.getString(R.string.confirm_cancel_download));
        dialog.setCallback(new ChoiceDialogCallback() {
            @Override
            public void onCanceled() {

            }

            @Override
            public void onAccepted() {
                downloadActions.cancelDownload(book);
            }
        });
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        ft.add(dialog, "ChoiceDialog");
        ft.commitAllowingStateLoss();
    }

    private void askRemoveQueue(DownloadActions downloadActions) {
        ChoiceDialog dialog = ChoiceDialog.newInstance(context.getString(R.string.confirm_remove_queue));
        dialog.setCallback(new ChoiceDialogCallback() {
            @Override
            public void onCanceled() {

            }

            @Override
            public void onAccepted() {
                downloadActions.removeBookQueue(book);
            }
        });
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        ft.add(dialog, "ChoiceDialog");
        ft.commitAllowingStateLoss();
    }
}



