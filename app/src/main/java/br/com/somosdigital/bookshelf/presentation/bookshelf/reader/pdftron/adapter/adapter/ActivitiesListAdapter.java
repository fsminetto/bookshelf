package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.adapter.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.solucaoadapta.shared.domain.model.activity.ActivityType;
import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;

public class ActivitiesListAdapter extends RecyclerView.Adapter<ActivitiesListAdapter.ActivitiesViewHolder> {

    private Context mContext;

    private ArrayList<BookActivityInfo> mActivities;

    private boolean isMultiSelection = false;

    private boolean canSendActivities = false;

    private IClickListener clickListener;

    public ActivitiesListAdapter(Context context, ArrayList<BookActivityInfo> objects,
                                 boolean canSendActivities, IClickListener clickListener) {
        mContext = context;
        mActivities = objects;
        this.canSendActivities = canSendActivities;
        this.clickListener = clickListener;
    }

    @Override
    public ActivitiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_activities_item, parent, false);
        return new ActivitiesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ActivitiesViewHolder mViewHolder, int position) {

        BookActivityInfo bookActivityInfo = mActivities.get(position);

        mViewHolder.activityTitle.setText(bookActivityInfo.getName());
        mViewHolder.activityPage.setText(String.format("Página(s): %s", bookActivityInfo.getPage()));

        mViewHolder.activityType.setImageResource(ActivityType.fromString(bookActivityInfo.getType()).getImage());

        if (bookActivityInfo.isSelected()) {
            mViewHolder.activitySelected.setVisibility(View.VISIBLE);
            mViewHolder.mainLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.book_activity_selected));
        } else {
            mViewHolder.activitySelected.setVisibility(View.GONE);
            mViewHolder.mainLayout.setBackground(ContextCompat.getDrawable(mContext, R.color.white));
        }

        mViewHolder.mainLayout.setOnClickListener(v -> clickListener.onItemClick(position));

        if (canSendActivities) {
            mViewHolder.mainLayout.setOnLongClickListener(v -> {
                isMultiSelection = true;
                clickListener.onItemLongClick(position);
                return true;
            });
        }

        mViewHolder.mainLayout.setOnTouchListener((v, event) -> {
            if (!isMultiSelection) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mViewHolder.mainLayout.setBackground(ContextCompat.getDrawable(mContext, R.color.adaptaLightBlue));
                        break;
                    case MotionEvent.ACTION_UP:
                        mViewHolder.mainLayout.setBackground(ContextCompat.getDrawable(mContext, R.color.white));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        mViewHolder.mainLayout.setBackground(ContextCompat.getDrawable(mContext, R.color.white));
                        break;
                }
            }
            return false;
        });


    }

    @Override
    public int getItemCount() {
        return mActivities != null ? mActivities.size() : 0;
    }

    public class ActivitiesViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayout;

        TextView activityTitle;

        TextView activityPage;

        ImageView activityType;

        ImageView activitySelected;

        public ActivitiesViewHolder(View itemView) {
            super(itemView);
            mainLayout = itemView.findViewById(R.id.book_activities_item_mainlayout);
            activityTitle = itemView.findViewById(R.id.book_activities_item_title);
            activityPage = itemView.findViewById(R.id.book_activities_item_pages);
            activityType = itemView.findViewById(R.id.book_activities_item_acttype);
            activitySelected = itemView.findViewById(R.id.book_activities_item_selected);
        }
    }

    public interface IClickListener {
        void onItemClick(int position);

        void onItemLongClick(int position);
    }
}

