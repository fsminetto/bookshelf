package br.com.somosdigital.bookshelf.domain.download.listeners;

import br.com.converge.download.android.DownloadStatusListener;
import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadCallback;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by marcus on 16/09/16.
 */
public class DownloadBookListener implements DownloadStatusListener {
    private Book book;

    private Log log;

    private DownloadCallback downloadCallback;

    private int checkProgress = 0;

    private long lastDownloadedBytes;

    public DownloadBookListener(Book book, DownloadCallback downloadCallback, Log log) {
        this.book = book;
        this.downloadCallback = downloadCallback;
        this.log = log;
    }

    @Override
    public void onDownloadComplete(int id) {
        log.logInfo("Download Book Complete");
        downloadCallback.unzipBook(book);
    }

    @Override
    public void onDownloadFailed(int id, int errorCode, String errorMessage) {
        log.logInfo("Download Book Failed");
        downloadCallback.bookError(book, null);
    }

    @Override
    public void onProgress(int id, long totalBytes, long downloadedBytes, int progress) {

        if (checkProgress + 2 == progress) {
            book.setCurrentBytesDownloaded(book.getCurrentBytesDownloaded()
                    + (downloadedBytes - lastDownloadedBytes));

            if(book.getCardProgress() != null) {
                book.getCardProgress()
                        .onProgressUpdate((DownloadInformation.getProgress(book)));
            }

            checkProgress = progress;
            lastDownloadedBytes = downloadedBytes;
        }
    }

    @Override
    public void onPause(int id, long downloadedBytes) {
        log.logInfo("Download Book Pause");

        DownloadInformation.setCurrentBookId("");
        book.setDownloadStatus(DownloadStatus.PAUSED);
        book.setGlobalDownloadStatus(DownloadStatus.PAUSED);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
        downloadCallback.nextBook();
    }

    @Override
    public void onDownloadCanceled(int id) {
        log.logInfo("Download Book Canceled");

        DownloadInformation.setCurrentBookId("");
        book.setDownloadStatus(DownloadStatus.CANCELING);
        book.setGlobalDownloadStatus(DownloadStatus.CANCELING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        removeBookFiles();

        book.setDownloadStatus(DownloadStatus.NEW);

        if (book.getActivity() != null) {
            book.getActivity().setDownloadStatus(DownloadStatus.NEW);
        }

        book.setGlobalDownloadStatus(DownloadStatus.NEW);
        book.setCurrentBytesDownloaded(0);

        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        downloadCallback.nextBook();

    }

    private void removeBookFiles() {
        FileManager.deleteFile(book.getCompactedPath());
        FileManager.removeDirectory(book.getTempPath());
        FileManager.deleteFile(book.getBrkPath());
        FileManager.removeDirectory(book.getBookPath());
    }
}
