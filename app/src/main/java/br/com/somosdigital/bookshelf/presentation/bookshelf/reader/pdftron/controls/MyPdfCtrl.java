package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.pdftron.common.PDFNetException;
import com.pdftron.pdf.Annot;
import com.pdftron.pdf.PDFViewCtrl;

/**
 * Created by jgsmanaroulas on 19/01/17.
 */
public class MyPdfCtrl extends PDFViewCtrl {

    private static final float TAP_REGION_THRESHOLD = (1f / 7f);
    private OnSingleTapListener listener;
    private boolean isAnnotationsEnabled;

    public void setSingleTapListener(OnSingleTapListener listener) {
        this.listener = listener;
    }

    public void setAnnotationsEnabled(boolean annotationsEnabled) {
        isAnnotationsEnabled = annotationsEnabled;
    }

    public interface OnSingleTapListener {
        void onSingleTapConfirmed(RegionSingleTap region);
    }

    public MyPdfCtrl(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent var1){
        int x = (int) (var1.getX() + 0.5);
        int y = (int) (var1.getY() + 0.5);


        if (isAnnotationsEnabled) {
            Annot annot = getAnnotationAt(x, y);
            try {
                if (annot != null && annot.isValid()) {
                    return super.onSingleTapConfirmed(var1);
                }
            } catch (PDFNetException e) {
                e.printStackTrace();
            }
        }

        RegionSingleTap region = getRegionTap(x, y);
        if (listener != null){
            listener.onSingleTapConfirmed(region);
            return true;
        }
        return false;
    }

    public enum RegionSingleTap {
        Left,
        Middle,
        Right
    }

    private RegionSingleTap getRegionTap(int x, int y) {
        RegionSingleTap regionSingleTap;

        float width = this.getWidth();
        float widthThresh = width * TAP_REGION_THRESHOLD;
        if (x <= widthThresh) {
            regionSingleTap = RegionSingleTap.Left;
        } else if (x >= width - widthThresh) {
            regionSingleTap = RegionSingleTap.Right;
        } else {
            regionSingleTap = RegionSingleTap.Middle;
        }

        return regionSingleTap;
    }
}
