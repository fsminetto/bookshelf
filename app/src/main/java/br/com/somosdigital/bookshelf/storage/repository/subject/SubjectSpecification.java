package br.com.somosdigital.bookshelf.storage.repository.subject;

import br.com.solucaoadapta.shared.domain.repository.Repository;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Subject;

/**
 * Created by pelisoli on 16/09/16.
 */
public interface SubjectSpecification extends Repository<Subject> {
}
