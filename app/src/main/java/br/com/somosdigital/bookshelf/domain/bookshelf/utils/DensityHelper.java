package br.com.somosdigital.bookshelf.domain.bookshelf.utils;

import android.content.Context;

/**
 * Created by marcus.martins on 26/06/15.
 */
public class DensityHelper {

    public static String getDensityName(Context context){
        float density = 0;

        if(context != null){
            density = context.getResources().getDisplayMetrics().density;
        }

        return getDensityName(density);
    }

    public static String getDensityName(float density) {
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "mdpi";
    }
}
