package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;

/**
 * Created by pelisoli on 19/09/16.
 */
public class Device {
    @Expose()
    private String DeviceId;

    @Expose()
    private String OperationalSystem;

    @Expose()
    private String VersionSystem;

    @Expose()
    private String DeviceModel;

    @Expose()
    private String UserSystem;

    @Expose()
    private String ScreenArea;

    public Device(String deviceId, String operationalSystem, String versionSystem, String deviceModel, String userSystem, String screenArea) {
        DeviceId = deviceId;
        OperationalSystem = operationalSystem;
        VersionSystem = versionSystem;
        DeviceModel = deviceModel;
        UserSystem = userSystem;
        ScreenArea = screenArea;
    }

    public Device() {}

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getOperationalSystem() {
        return OperationalSystem;
    }

    public void setOperationalSystem(String operationalSystem) {
        OperationalSystem = operationalSystem;
    }

    public String getVersionSystem() {
        return VersionSystem;
    }

    public void setVersionSystem(String versionSystem) {
        VersionSystem = versionSystem;
    }

    public String getDeviceModel() {
        return DeviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        DeviceModel = deviceModel;
    }

    public String getUserSystem() {
        return UserSystem;
    }

    public void setUserSystem(String userSystem) {
        UserSystem = userSystem;
    }

    public String getScreenArea() {
        return ScreenArea;
    }

    public void setScreenArea(String screenArea) {
        ScreenArea = screenArea;
    }
}
