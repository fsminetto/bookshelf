package br.com.somosdigital.bookshelf.storage.repository.grade;


import br.com.somosdigital.bookshelf.domain.model.distribuidor.Grade;
import io.realm.Realm;

/**
 * Created by pelisoli on 16/09/16.
 */
public class GradeRepository implements GradeSpecification {
    Realm realm;

    public GradeRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(Grade item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Grade> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Grade item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Grade> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Grade item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realm1 -> item.deleteFromRealm());
        }
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}
