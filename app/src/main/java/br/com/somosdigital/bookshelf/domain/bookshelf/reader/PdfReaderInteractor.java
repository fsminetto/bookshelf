package br.com.somosdigital.bookshelf.domain.bookshelf.reader;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.converge.encryption.android.RSA;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.solucaoadapta.shared.storage.preferences.LocalPreferences;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookFile;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Device;
import br.com.somosdigital.bookshelf.domain.utils.DrmClient;
import br.com.somosdigital.bookshelf.mapper.OedInfoMapper;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderContract;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.BookActivityInfo;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;
import br.com.somosdigital.bookshelf.service.distribuidor.DistribuidorService;
import br.com.somosdigital.bookshelf.storage.module.BookshelfDatabase;
import br.com.somosdigital.bookshelf.storage.repository.book.BookRepository;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderContract.XFDF_TAG_ADD;
import static br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderContract.XFDF_TAG_DELETE;
import static br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.PdfReaderContract.XFDF_TAG_MODIFY;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_ADD_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_ADD_START;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_DELETE_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_DELETE_START;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_MODIFY_END;
import static com.pdftron.pdf.tools.AnnotCollabManager.XFDF_TAG_MODIFY_START;

/**
 * Created by fsminetto on 21/12/17.
 */

public class PdfReaderInteractor implements PdfReaderContract.Interactor {

    private String bookRoot;
    private String bookId;
    private String userId;
    private Device device;
    private String cryptography;
    private Log log;
    private DistribuidorService service;
    private String changeFilename;
    private final LocalPreferences localPreferences;
    private boolean hasActivities;
    private boolean showActivities;

    public PdfReaderInteractor(String bookRoot,
                               String bookId,
                               String userId,
                               Device device,
                               String cryptography,
                               Log log,
                               LocalPreferences localPreferences,
                               boolean hasActivities,
                               boolean showActivities,
                               DistribuidorService service) {
        this.device = device;
        this.cryptography = cryptography;
        this.bookRoot = bookRoot;
        this.bookId = bookId;
        this.userId = userId;
        this.log = log;
        this.localPreferences = localPreferences;
        this.hasActivities = hasActivities;
        this.showActivities = showActivities;
        this.service = service;

        changeFilename = bookRoot + File.separator + "annotations" + File.separator + "changes_" + userId + ".xfdf";
    }

    @Override
    public String getActivitiesFilePath() {
        return bookRoot + "/activities/activities.json";
    }

    @Override
    public HashMap<Integer, List<BookActivityInfo>> getActivityMap() throws JSONException {
        HashMap<Integer, List<BookActivityInfo>> activityMap = new HashMap<>();
        if (hasActivities && showActivities) {
            String activitiesPath = getActivitiesFilePath();
            JSONArray array = new JSONArray(readFile(activitiesPath));
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                if (object.has("Page")) {
                    int page = object.getInt("Page");
                    if (object.has("activities")) {
                        List<BookActivityInfo> activityList = new ArrayList<>();
                        Gson gson = new Gson();
                        JSONArray activitiesArray = object.getJSONArray("activities");
                        for (int j = 0; j < activitiesArray.length(); j++) {
                            BookActivityInfo activity = gson.fromJson(activitiesArray.getJSONObject(j).toString(), BookActivityInfo.class);
                            activityList.add(activity);
                        }
                        activityMap.put(page, activityList);
                    }
                }
            }
        }
        return activityMap;
    }

    private Book getBook() {
        Realm realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());
        BookRepository bookRepository = new BookRepository(realm);
        Book book = bookRepository.getById(bookId);
        realm.close();
        return book;
    }

    @Override
    public Observable<BookFile> getBookFile() {
        Observable<BookFile> result;

        Book book = getBook();

        if (book != null) {
            BookFile postBookFile = new BookFile();
            postBookFile.setBookId(book.getId());
            postBookFile.setKeyCryptography(cryptography);
            postBookFile.setDevice(device);
            postBookFile.setFileType(book.getFileType());
            if (service != null) {
                result = service.getBookFile(postBookFile).subscribeOn(Schedulers.io());
            } else {
                result = Observable.error(new Throwable("APS instance is null"));
            }
        } else {
            result = Observable.error(new Throwable("No book found with that ID"));
        }

        return result;
    }

    @Override
    public List<OedInfo> getOedList() {
        Book book = getBook();

        List<OedInfo> oedList = new ArrayList<>();
        if (book != null) {
            if (book.getEducationalObjects() != null && book.getEducationalObjects().size() > 0) {
                oedList = OedInfoMapper.mapToOedInfo(book.getEducationalObjects());
            }
        }
        return oedList;
    }

    @Override
    public Observable<Response<ResponseBody>> downloadAnnotationFile() {
        Observable<Response<ResponseBody>> result;
        if (service != null) {
            result = service
                    .getXFDFFile(bookId)
                    .subscribeOn(Schedulers.io())
                    .doOnNext(responseBody -> saveXFDF(responseBody, bookRoot, userId))
                    .doOnError(throwable -> {
                        log.logError("DEU RUIM NO download!");
                        log.logError(throwable.getLocalizedMessage());
                    });
        } else {
            result = Observable.error(new Throwable("APS instance is null"));
        }

        return result;
    }

    private void saveXFDF(Response<ResponseBody> responseBody, String bookRoot, String userId) {
        if (responseBody.isSuccessful()) {
            log.logDebug("RECEIVED XFDF FILE SUCCESSFULLY");
            String filePath = bookRoot + File.separator + "annotations" + File.separator + userId + ".xfdf";
            if (saveFileToDisk(filePath, responseBody.body())) {
                log.logDebug(readFile(filePath));
                log.logDebug("SAVED XFDF FILE TO DISK");
            } else {
                log.logError("FAILED TO SAVE FILE TO DISK");
            }
        } else {
            log.logError("FAILED TO GET FILE FROM XFDF SERVER");
        }
    }

    @Override
    public boolean checkIfDocumentExists() {
        try {
            FileInputStream fis = new FileInputStream(getBookFilePath(bookId));
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getBookFilePath(String id) {
        return bookRoot + "/" + id + ".pdf";
    }

    @Override
    public List<BookActivityInfo> loadPageActivities(int[] visiblePages, HashMap<Integer, List<BookActivityInfo>> activityMap) {
        if (hasActivities && showActivities && visiblePages.length > 0) {
            ArrayList<BookActivityInfo> activitiesList = new ArrayList<>();

            for (int i : visiblePages) {
                activitiesList.addAll(getPageActivities(i, activityMap));
            }
            return activitiesList;

        }
        return null;
    }

    private ArrayList<BookActivityInfo> getPageActivities(int page, HashMap<Integer, List<BookActivityInfo>> activityMap) {
        if (activityMap.containsKey(page)) {
            return new ArrayList<>(activityMap.get(page));
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public int getLastReadPage() {
        int lastPage = 1;
        if (localPreferences != null && userId != null && bookId != null) {

            String currentBookLastPages = localPreferences.getString(bookId);
            if (currentBookLastPages != null) {
                try {
                    JSONObject object = new JSONObject(currentBookLastPages);
                    if (object.has(userId)) {
                        lastPage = object.getInt(userId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    log.logError("getLastReadPage: " + e.getMessage());
                }
            }
        }
        return lastPage;
    }

    @Override
    public void saveLastReadPage(int currentPage) {

        if (localPreferences != null) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(userId, currentPage);

                String currentBookLastPages = localPreferences.getString(bookId);

                if (currentBookLastPages == null) {
                    localPreferences.setString(bookId, jsonObject.toString());
                } else {
                    JSONObject storedJsonPages = new JSONObject(currentBookLastPages);
                    storedJsonPages.put(userId, currentPage);
                    localPreferences.setString(bookId, storedJsonPages.toString());
                }

            } catch (JSONException e) {
                log.logError("saveLastReadPage: " + e.getMessage());
            }
        }
    }

    @Override
    public String getDocumentPassword(DrmClient drmClient) {
        String password = null;
        File file = new File(bookRoot + File.separator + bookId + ".brk");

        try {
            if (file.exists()) {
                int size = (int) file.length();
                byte[] bytes = new byte[size];

                if (size > 0) {
                    BufferedInputStream buf =
                            new BufferedInputStream(new FileInputStream(file));
                    buf.read(bytes, 0, bytes.length);
                    buf.close();

                    password = new RSA().decrypt(new String(bytes),
                            drmClient.getKeys().getPrivateKey());
                }
            }
        } catch (Exception e) {
            if (e != null && e.getStackTrace() != null) {
                log.logError(e.getMessage());
            }
        }
        return password;
    }

    public String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void removeAnnotChangesFile() {
        File file = new File(changeFilename);
        if (file.exists())
            file.delete();
    }

    @Override
    public void discarAnnotationsBackup() {
        localPreferences.setObjectToJson(null, PdfReaderContract.addName);
        localPreferences.setObjectToJson(null, PdfReaderContract.modifyName);
        localPreferences.setObjectToJson(null, PdfReaderContract.removeName);
    }

    @Override
    public Map<String, String> getChangeList(String name) {
        Map<String, String> changes = ((HashMap<String, String>) localPreferences.getObjectFromJson(name, HashMap.class));
        return changes != null ? changes : new HashMap<>();
    }

    @Override
    public void registerAnnotation(String name, Map<String, String> annotations, String uuid, String xfdfCommand, String tagStart, String tagEnd) {
        annotations.put(uuid, extractCommand(tagStart, tagEnd, xfdfCommand));
        localPreferences.setObjectToJson(annotations, name);
    }

    private String extractCommand(String openTag, String closeTag, String command) {
        int indexStart = command.indexOf(openTag);
        int indexEnd = command.indexOf(closeTag);
        if (indexStart > 0 && indexEnd > 0) {
            return command.substring(indexStart + openTag.length(), indexEnd);
        }
        return "";
    }

    @Override
    public String createXFDFString(Map<String, String> addCommands, Map<String, String> modifyCommands, Map<String, String> removeCommands) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n" +
                createCommandBlock(XFDF_TAG_ADD_START, XFDF_TAG_ADD_END, XFDF_TAG_ADD, addCommands.values()) +
                createCommandBlock(XFDF_TAG_MODIFY_START, XFDF_TAG_MODIFY_END, XFDF_TAG_MODIFY, modifyCommands.values()) +
                createCommandBlock(XFDF_TAG_DELETE_START, XFDF_TAG_DELETE_END, XFDF_TAG_DELETE, removeCommands.values()) +
                "<pdf-info version=\"2\" xmlns=\"http://www.pdftron.com/pdfinfo\" />\n</xfdf>";
    }

    private String createCommandBlock(String openTag, String closeTag, String emptyTag, Collection<String> commandList) {
        StringBuilder commandBlock = new StringBuilder("");
        if (commandList.size() > 0) {
            commandBlock.append(openTag);
            for (String command : commandList) {
                commandBlock.append(command);
            }
            commandBlock.append(closeTag);
            commandBlock.append("\n");
        } else {
            commandBlock.append(emptyTag);
            commandBlock.append("\n");
        }
        return commandBlock.toString();
    }

    @Override
    public Observable<Response<ResponseBody>> sendAnnotations(String xfdfCommand) {
        Observable<Response<ResponseBody>> result;
        File file = createChangeFile();
        try {
            if (file.exists()) file.delete();
            file.createNewFile();

            PrintWriter writer = new PrintWriter(file);
            writer.write(xfdfCommand);
            writer.flush();
            writer.close();

            result = service
                    .sendXFDFFile(bookId, RequestBody.create(MediaType.parse("text/xml; charset=utf-8"), file))
                    .subscribeOn(Schedulers.io())
                    .doOnNext(responseBody -> {
                        file.delete();
                        saveXFDF(responseBody, bookRoot, userId);
                    })
                    .doOnError(throwable -> {
                        file.delete();
                        log.logError("Erro ao enviar arquivo de diff");
                        log.logError(throwable.getLocalizedMessage());
                    });

        } catch (Exception e) {
            file.delete();
            e.printStackTrace();
            result = Observable.error(e);

        }

        return result;
    }

    private File createChangeFile() {
        File file = new File(changeFilename);
        if (!file.getParentFile().exists())
            file.mkdirs();
        return file;
    }

    private boolean saveFileToDisk(String filePath, ResponseBody body) {
        try {
            File newFile = new File(filePath);
            newFile.getParentFile().mkdirs();
            if (newFile.exists()) {
                newFile.delete();
            }

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(newFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    log.logDebug("file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                log.logError(e.getMessage());
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            log.logError(e.getMessage());
            return false;
        }
    }

    @Override
    public String readChangeFile() {
        return readFile(changeFilename);
    }
}
