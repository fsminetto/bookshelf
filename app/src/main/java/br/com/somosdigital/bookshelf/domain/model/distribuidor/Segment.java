package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jgsmanaroulas on 13/03/17.
 */
public class Segment extends RealmObject{

    @PrimaryKey
    @Expose()
    private String id;

    @Expose()
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
