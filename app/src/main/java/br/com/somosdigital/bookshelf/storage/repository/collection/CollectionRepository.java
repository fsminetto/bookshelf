package br.com.somosdigital.bookshelf.storage.repository.collection;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Collection;
import io.realm.Realm;

/**
 * Created by pelisoli on 16/09/16.
 */
public class CollectionRepository implements CollectionSpecification {
    Realm realm;

    public CollectionRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(Collection item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Collection> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Collection item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Collection> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Collection item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realm1 -> item.deleteFromRealm());
        }
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}
