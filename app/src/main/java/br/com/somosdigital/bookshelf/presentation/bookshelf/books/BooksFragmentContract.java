package br.com.somosdigital.bookshelf.presentation.bookshelf.books;

import java.util.List;

import br.com.solucaoadapta.shared.presentation.base.MvpPresenter;
import br.com.solucaoadapta.shared.presentation.base.MvpView;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Template;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.CardViewHolder;
import br.com.somosdigital.bookshelf.service.template.TemplateService;
import br.com.somosdigital.bookshelf.storage.repository.template.TemplateSpecification;

/**
 * Created by marcus on 19/10/16.
 */

public interface BooksFragmentContract {
    interface View extends MvpView {
        void showBooks(List<Book> bookList);

        void showFilterBooks(List<Book> bookList);

        void showFilterSubjects(List<Book> bookList);

        void showBookMenu(CardViewHolder cardViewHolder, Book book);

        void openBook(Book book);

        void onDeviceLimitReachError();

        void onDeviceBlockedError();

        void onBookExpired();

    }

    interface Presenter extends MvpPresenter<View> {
        BookshelfUser loadUser(String userId);

        void loadBooks(BookshelfUser bookshelfUser, String deviceId, String screenDensity);

        void checkTemplateUpdate(TemplateService activityService,
                                 TemplateSpecification templateSpecification,
                                 RequestCreator requestCreator,
                                 Template newTemplate);

        void searchByTitle(List<Book> bookList, String text);

        void searchBySubject(List<Book> bookList, String text);
    }
}
