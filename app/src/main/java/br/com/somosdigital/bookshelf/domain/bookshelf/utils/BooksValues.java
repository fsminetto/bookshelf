package br.com.somosdigital.bookshelf.domain.bookshelf.utils;

/**
 * Created by pelisoli on 29/03/16.
 */
public class BooksValues {
    private static BooksValues instance = null;

    private String bookRootPath;

    private String templatePath;

    public static BooksValues getInstance(){
        if(instance == null){
            instance = new BooksValues();
        }

        return instance;
    }

    public static void setInstance(BooksValues booksValues){
        instance = booksValues;
    }

    public String getBookRootPath() {
        return bookRootPath;
    }

    public void setBookRootPath(String bookRootPath) {
        this.bookRootPath = bookRootPath;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

}
