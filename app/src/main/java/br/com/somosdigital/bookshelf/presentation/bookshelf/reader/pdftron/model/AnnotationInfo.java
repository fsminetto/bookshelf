package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model;

import com.pdftron.pdf.Annot;

/**
 * Created by jgsmanaroulas on 11/04/17.
 */
public class AnnotationInfo {
    /**
     * The annotation type is one of the types found in com.pdftron.pdf.Annot.
     */
    private int mType;

    /**
     * Holds the page where this annotation is found.
     */
    private int mPageNum;

    /**
     * The contents of the annotation are used in the list view of the
     * BookmarkDialogFragment.
     */
    private String mContent;

    /**
     * The author for this annotation.
     */
    private String mAuthor;

    private Annot mAnnotation;

    /**
     * Default constructor. Creates an empty annotation entry.
     */
    public AnnotationInfo() {
        // TODO Maybe -1, -1, "", ""?
        this(0, 0, "", "", null);
    }

    /**
     * Class constructor specifying the type, page and content of the
     * annotation.
     *
     * @param type    the type of the annotation
     * @param pageNum the page where this annotation lies in
     * @param content the content of the annotation
     * @see <a href="http://www.pdftron.com/pdfnet/mobile/Javadoc/pdftron/PDF/Annot.html">Class Annot</a>
     */
    public AnnotationInfo(int type, int pageNum, String content, String author, Annot annotation) {
        this.mType = type;
        this.mPageNum = pageNum;
        this.mContent = content;
        this.mAuthor = author;
        this.mAnnotation = annotation;
    }

    /**
     * Get the type of this annotation.
     *
     * @return the type of the annotation
     * @see {@link com.pdftron.pdf.Annot}
     */
    public int getType() {
        return mType;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public int getPageNum() {
        return mPageNum;
    }

    public void setPageNum(int mPageNum) {
        this.mPageNum = mPageNum;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String mContent) {
        this.mContent = mContent;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        this.mAuthor = author;
    }

    public Annot getAnnotation() {
        return mAnnotation;
    }
}
