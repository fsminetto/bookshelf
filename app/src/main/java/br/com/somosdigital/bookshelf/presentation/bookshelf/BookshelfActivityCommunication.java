package br.com.somosdigital.bookshelf.presentation.bookshelf;

import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by marcus on 01/12/16.
 */

public interface BookshelfActivityCommunication {
    void startReader(Book book, boolean hasActivities);

    void showBookInformation(String bookId);
}
