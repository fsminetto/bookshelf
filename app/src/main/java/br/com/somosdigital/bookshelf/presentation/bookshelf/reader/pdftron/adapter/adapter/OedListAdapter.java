package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.adapter.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model.OedInfo;

public class OedListAdapter extends ArrayAdapter<OedInfo> {

    private Context mContext;

    private int mLayoutResourceId;

    private List<OedInfo> mOeds;

    private ViewHolder mViewHolder;

    public OedListAdapter(Context context, int resource, List<OedInfo> objects) {
        super(context, resource, objects);
        mContext = context;
        mLayoutResourceId = resource;
        mOeds = objects;
    }

    @Override
    public int getCount() {
        if (mOeds != null) {
            return mOeds.size();
        } else {
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(mLayoutResourceId, null);

            mViewHolder = new ViewHolder();
            mViewHolder.oedTitle = convertView.findViewById(R.id.book_oeds_item_title);
            mViewHolder.oedPage = convertView.findViewById(R.id.book_oeds_item_page);

            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        OedInfo oedInfo = mOeds.get(position);

        mViewHolder.oedTitle.setText(oedInfo.getName());
        mViewHolder.oedPage.setText(oedInfo.getPages() != null ? mContext.getString(R.string.pages, oedInfo.getPages()) : "");

        return convertView;
    }

    private class ViewHolder {
        public TextView oedTitle;
        public TextView oedPage;
    }
}

