package br.com.somosdigital.bookshelf.domain.model.distribuidor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.presentation.bookshelf.books.card.ProgressAction;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by marcus.martins on 06/09/16.
 */
public class Book extends RealmObject{
    @Expose()
    @PrimaryKey
    @SerializedName("id")
    private String id;

    @Expose()
    private String title;

    @Expose()
    private String keyCryptography;

    @Expose()
    private String coverPath;

    @Expose()
    @Ignore
    private String filePath;

    @Expose()
    @Ignore
    private List<Author> authors;

    @Expose()
    private String authorsNames;

    @Expose()
    private Collection collection;

    @Expose()
    private Grade grade;

    @Expose()
    @SerializedName(value = "subject")
    private RealmList<Subject> subjectList;

    @Expose()
    private Segment segment;

    @Expose()
    private int version;

    @Expose()
    @Ignore
    private Device device;

    @Expose()
    @SerializedName(value = "file_length")
    private long fileLength;

    @Expose()
    private RealmList<Oed> educationalObjects;

    @Expose()
    private Activity activity;

    @Expose()
    private int downloadStatus;

    @Expose()
    private int globalDownloadStatus;

    @Expose()
    private String bookPath;

    @Expose()
    private String compactedPath;

    @Expose()
    private String tempPath;

    @Expose()
    private String brkPath;

    @Expose()
    private boolean expires;

    @Expose()
    private boolean expiredDistribution;

    @Expose()
    private String endDateDistribution;

    @Expose()
    private boolean allowOedDownload;

    @Ignore
    private boolean inQueue;

    @Ignore
    private int bookPosition;

    @Ignore
    private transient ProgressAction cardProgress;

    private float totalBytesToDownload;

    private float currentBytesDownloaded;

    @Expose()
    @Ignore
    private boolean update = false;

    @Expose()
    @SerializedName(value = "oed_length")
    private long oedLength;

    @Expose()
    @Ignore
    private BookFile file;

    @Expose()
    @Ignore
    @SerializedName(value = "file_type")
    private String fileType = "pdf";

    @Expose(serialize = false)
    @SerializedName(value = "primary_info")
    private String primaryInfo;

    @Expose(serialize = false)
    @SerializedName(value = "secondary_info")
    private String secondaryInfo;

    private int downloadType;

    @Expose()
    @SerializedName(value = "type_book")
    private String bookType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyCryptography() {
        return keyCryptography;
    }

    public void setKeyCryptography(String keyCryptography) {
        this.keyCryptography = keyCryptography;
    }

    public String getCoverPath() {
        return coverPath;
    }

    public void setCoverPath(String coverPath) {
        this.coverPath = coverPath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public RealmList<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(RealmList<Subject> subjects) {
        this.subjectList = subjects;
    }

    public Segment getSegment() {
        return segment;
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public RealmList<Oed> getEducationalObjects() {
        return educationalObjects;
    }

    public void setEducationalObjects(RealmList<Oed> educationalObjects) {
        this.educationalObjects = educationalObjects;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        if (downloadStatus != null) {
            this.downloadStatus = downloadStatus.getId();
        }
    }

    public DownloadStatus getDownloadStatus() {
        return DownloadStatus.fromInt(this.downloadStatus);
    }

    public void setGlobalDownloadStatus(DownloadStatus downloadStatus) {
        this.globalDownloadStatus = downloadStatus.getId();
    }

    public DownloadStatus getGlobalDownloadStatus() {
        return DownloadStatus.fromInt(this.globalDownloadStatus);
    }

    public boolean isInQueue() {
        return inQueue;
    }

    public void setInQueue(boolean inQueue) {
        this.inQueue = inQueue;
    }

    public int getBookPosition() {
        return bookPosition;
    }

    public void setBookPosition(int bookPosition) {
        this.bookPosition = bookPosition;
    }

    public String getBookPath() {
        return bookPath;
    }

    public void setBookPath(String bookPath) {
        this.bookPath = bookPath;
    }

    public String getCompactedPath() {
        return compactedPath;
    }

    public void setCompactedPath(String compactedPath) {
        this.compactedPath = compactedPath;
    }

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getBrkPath() {
        return brkPath;
    }

    public void setBrkPath(String brkPath) {
        this.brkPath = brkPath;
    }

    public ProgressAction getCardProgress() {
        return cardProgress;
    }

    public void setCardProgress(ProgressAction cardProgress) {
        this.cardProgress = cardProgress;
    }

    public float getTotalBytesToDownload() {
        return totalBytesToDownload;
    }

    public void setTotalBytesToDownload(float totalBytesToDownload) {
        this.totalBytesToDownload = totalBytesToDownload;
    }

    public float getCurrentBytesDownloaded() {
        return currentBytesDownloaded;
    }

    public void setCurrentBytesDownloaded(float currentBytesDownloaded) {
        this.currentBytesDownloaded = currentBytesDownloaded;
    }

    public String getDisplayBookTitle() {
        StringBuilder subjects = new StringBuilder();
        if (subjectList != null && subjectList.size() > 0){
            for (int i = 0; i< subjectList.size(); i++){
                if(i == 0){
                    subjects.append(subjectList.get(i).getName());
                }else{
                    subjects.append(", ").append(subjectList.get(i).getName());
                }
            }
        }

        return String.format("%1s %2s",
                subjects.toString(),
                (grade != null ? grade.getDescription() : ""));
    }

    public String getDisplayBookSubjects() {
        StringBuilder subjects = new StringBuilder();
        if (subjectList != null && subjectList.size() > 0){
            for (int i = 0; i< subjectList.size(); i++){
                if(i == 0){
                    subjects.append(subjectList.get(i).getName());
                }else{
                    subjects.append(", ").append(subjectList.get(i).getName());
                }
            }
        }

        return subjects.toString();
    }

    public String getDisplayMatrixBookTitle() {
        StringBuilder subjects = new StringBuilder();

        if (subjectList != null && subjectList.size() > 0){
            if(subjectList.size() > 1){
                //TODO it is wrong. Put string on strings.xml
                subjects.append("Apostilas");
            }else{
                subjects.append(subjectList.get(0).getName());
            }
        }

        return String.format("%1s %2s - %3s",
                subjects,
                (grade != null ? grade.getDescription() : ""),
                (collection != null ? collection.getName() : ""));
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public long getOedLength() {
        return oedLength;
    }

    public void setOedLength(long oedLength) {
        this.oedLength = oedLength;
    }

    public BookFile getFile() {
        return file;
    }

    public void setFile(BookFile file) {
        this.file = file;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public int getDownloadType() {
        return downloadType;
    }

    public void setDownloadType(int downloadType) {
        this.downloadType = downloadType;
    }

    public boolean isExpires() {
        return expires;
    }

    public void setExpires(boolean expires) {
        this.expires = expires;
    }

    public boolean isExpiredDistribution() {
        return expiredDistribution;
    }

    public void setExpiredDistribution(boolean expiredDistribution) {
        this.expiredDistribution = expiredDistribution;
    }

    public String getEndDateDistribution() {
        return endDateDistribution;
    }

    public void setEndDateDistribution(String endDateDistribution) {
        this.endDateDistribution = endDateDistribution;
    }

    public boolean isAllowOedDownload() {
        return allowOedDownload;
    }

    public void setAllowOedDownload(boolean allowOedDownload) {
        this.allowOedDownload = allowOedDownload;
    }

    public String getPrimaryInfo() {
        return primaryInfo;
    }

    public void setPrimaryInfo(String primaryInfo) {
        this.primaryInfo = primaryInfo;
    }

    public String getSecondaryInfo() {
        return secondaryInfo;
    }

    public void setSecondaryInfo(String secondaryInfo) {
        this.secondaryInfo = secondaryInfo;
    }

    public String getAuthorsNames() {
        return authorsNames;
    }

    public void setAuthorsNames(String authorsNames) {
        this.authorsNames = authorsNames;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }

    public boolean isTeacherBook() {
        boolean result = false;

        if (getBookType() != null) {
            result = getBookType().equalsIgnoreCase("TEACHER");
        }

        return result;
    }
}
