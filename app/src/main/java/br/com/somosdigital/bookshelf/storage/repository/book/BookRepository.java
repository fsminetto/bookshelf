package br.com.somosdigital.bookshelf.storage.repository.book;

import android.util.Log;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import br.com.solucaoadapta.shared.storage.runtime.ClassStatusManager;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.BookshelfUser;
import br.com.somosdigital.bookshelf.storage.module.BookshelfDatabase;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * Created by pelisoli on 16/09/16.
 */
public class BookRepository implements BookSpecification {
    private Realm realm;

    public BookRepository() {
    }

    public BookRepository(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void add(Book item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(item));
        }
    }

    @Override
    public void add(Iterable<Book> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealm(items));
        }
    }

    @Override
    public void update(Book item) {
        if(checkRealmInstance(realm)){
            Timber.e("Realm Size: " + new File(realm.getPath()).length());
            Timber.e("Realm open instances: " + Realm.getGlobalInstanceCount(realm.getConfiguration()));
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(item));
        }
    }

    @Override
    public void update(Iterable<Book> items) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realmInstance -> realmInstance.copyToRealmOrUpdate(items));
        }
    }

    @Override
    public void remove(Book item) {
        if(checkRealmInstance(realm)){
            realm.executeTransaction(realm1 -> {
                String userId = ClassStatusManager.getInstance().getCurrentUser().getId();
                RealmResults<BookshelfUser> bookshelfUsers = realm1.where(BookshelfUser.class)
                                                                    .equalTo("id", userId)
                                                                    .findAll()
                                                                    .where()
                                                                    .equalTo("books.id", item.getId())
                                                                    .findAll();
                if (bookshelfUsers.size() > 0) {
                    for (BookshelfUser user : bookshelfUsers) {
                        Log.d("usersbook size", "size = " + user.getBooks().size());
                        Iterator<Book> bookIterator = user.getBooks().iterator();
                        while (bookIterator.hasNext()) {
                            Book book = bookIterator.next();
                            if (book.getId().equalsIgnoreCase(item.getId())) {
                                bookIterator.remove();
                            }
                        }
                        Log.d("usersbook size", "size = " + user.getBooks().size());
                    }
                }

                long usersWithBook = realm1.where(BookshelfUser.class)
                                            .equalTo("books.id", item.getId())
                                            .count();
                if (usersWithBook == 0) {
                    RealmResults<Book> books = realm1.where(Book.class).equalTo("id", item.getId()).findAll();
                    for (Book book : books) {
                        File f = new File(book.getBookPath());
                        if (f.exists()) f.delete();
                    }
                    books.deleteAllFromRealm();
                }

            });
        }
    }

    @Override
    public RealmList<Book> getBooks() {
        RealmList<Book> result = new RealmList<>();

        if (checkRealmInstance(realm)) {
            List<Book> list = realm.copyFromRealm(realm.where(Book.class).findAll());

            if (list != null && list.size() > 0) {
                result.addAll(list);
            }
        }

        return result;
    }

    @Override
    public RealmList<Book> getDownloadedBooks() {
        RealmList<Book> result = new RealmList<>();

        if(checkRealmInstance(realm)) {
            List<Book> list = realm.copyFromRealm(realm.where(Book.class)
                    .equalTo("downloadStatus", DownloadStatus.SUCCESSFUL.getId())
                    .findAll()
                    .where()
                    .isNotNull("activity")
                    .findAll()
                    .where()
                    .equalTo("activity.downloadStatus", DownloadStatus.SUCCESSFUL.getId())
                    .findAll());

            if (list.size() > 0) {
                result.addAll(list);
            }
        }

        return result;
    }

    @Override
    public Book getById(String idBook) {
        Book result = null;

        if (checkRealmInstance(realm)) {
            Book databaseBook = realm.where(Book.class).equalTo("id", idBook).findFirst();

            if (databaseBook != null) {
                result = realm.copyFromRealm(databaseBook);
            }
        }

        return result;
    }

    @Override
    public void closeRealm() {
        if(realm != null && !realm.isClosed()){
            realm.close();
            realm = null;
        }
    }

    @Override
    public void openRealm() {
        if (realm == null) {
            realm = Realm.getInstance(BookshelfDatabase.getRealmConfig());
        }
    }

    private boolean checkRealmInstance(Realm realm){
        boolean status = false;

        if (realm != null) {
            status = true;
        }

        return status;
    }
}
