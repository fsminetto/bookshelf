package br.com.somosdigital.bookshelf.presentation.bookshelf.books.information;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pnikosis.materialishprogress.ProgressWheel;

import br.com.somosdigital.bookshelf.R;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;

/**
 * Created by jgsmanaroulas on 07/02/17.
 */
public class BookInformationDialog extends DialogFragment implements BookInformationContract.View {

    private static final String SCREEN_FRIENDLY_NAME = "Informações do Livro";

    private String bookId;

    private TextView txvCollection;

    private TextView txvSubject;

    private TextView txvYear;

    private TextView txvSegment;

    private TextView txvAuthor;

    private TextView txvBookSize;

    private TextView txvOedSize;

    private ImageView imvBookCover;

    private TextView txvClose;

    private TextView txvTitle;

    private RelativeLayout viewContent;

    private ProgressWheel loadingIcon;

    private BookInformationPresenter presenter;

    public static BookInformationDialog newInstance(String bookId) {
        BookInformationDialog bookInformationFragment = new BookInformationDialog();

        Bundle bundle = new Bundle();
        bundle.putString("bookId", bookId);

        bookInformationFragment.setArguments(bundle);

        return bookInformationFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.book_information, null);

        getBasicInfo(getArguments());
        findViewsById(view);

        presenter = new BookInformationPresenter();
        presenter.attachView(this);
        presenter.loadBook(bookId);

        return view;
    }

    private void findViewsById(View view) {
        txvCollection = view.findViewById(R.id.book_information_txv_colection);
        txvSubject = view.findViewById(R.id.book_information_txv_subject);
        txvYear = view.findViewById(R.id.book_information_txv_year);
        txvSegment = view.findViewById(R.id.book_information_txv_segment);
        txvAuthor = view.findViewById(R.id.book_information_txv_authors);
        txvBookSize = view.findViewById(R.id.book_information_txv_book_size);
        txvOedSize = view.findViewById(R.id.book_information_txv_oeds_size);
        imvBookCover = view.findViewById(R.id.book_information_cover);
        txvClose = view.findViewById(R.id.book_information_btn_close);
        txvTitle = view.findViewById(R.id.book_information_txv_title);
        viewContent = view.findViewById(R.id.content_layout);
        loadingIcon = view.findViewById(R.id.progress);
    }


    private void getBasicInfo(Bundle bundle){
        if (bundle != null) {
            bookId = bundle.getString("bookId");
        }
    }

    @Override
    public void showLoadingBookInfo(boolean show) {
        if(show) {
            loadingIcon.setVisibility(View.VISIBLE);
            viewContent.setVisibility(View.GONE);

        } else {
            loadingIcon.setVisibility(View.GONE);
            viewContent.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showBook(Book book) {
        long bookAndActivitySize;
        String title = "";
        String collection = "";
        String discipline = "";
        String segment = "";
        String year = "";

        Glide.with(this)
                .load(book.getCoverPath())
                .placeholder(R.color.no_book)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(imvBookCover);


        if (book.getCollection() != null){
            collection = book.getCollection().getDescription();
        }

        if (book.getDisplayBookSubjects() != null) {
            discipline = book.getDisplayBookSubjects();
        }

        if (book.getGrade() != null) {
            year = book.getGrade().getDescription();
        }

        if (book.getSegment() != null){
            segment = book.getSegment().getDescription();
        }

        if (book.getTitle() != null) {
            title = book.getTitle();
        }

        txvTitle.setText(title);
        txvCollection.setText(collection);
        txvSubject.setText(discipline);
        txvSegment.setText(segment);
        txvYear.setText(year);

        txvOedSize.setText(presenter.getReadableFileSize(book.getOedLength()));

        bookAndActivitySize = book.getFileLength() ;
        bookAndActivitySize += book.getActivity() != null ? book.getActivity().getFileLength() : 0;

        txvBookSize.setText(presenter.getReadableFileSize(bookAndActivitySize));

        txvAuthor.setText(book.getAuthorsNames());

        txvClose.setOnClickListener(view -> dismissAllowingStateLoss());
    }

    @Override
    public void showServerMaintenanceError() {

    }
}
