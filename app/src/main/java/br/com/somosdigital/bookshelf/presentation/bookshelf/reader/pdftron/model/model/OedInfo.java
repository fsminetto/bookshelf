package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jgsmanaroulas on 17/01/17.
 */
public class OedInfo implements Parcelable {

    private String id;

    private String name;

    private String pages;

    private boolean allowOedDownload;

    private String linkPreview;

    public OedInfo(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public boolean isAllowOedDownload() {
        return allowOedDownload;
    }

    public void setAllowOedDownload(boolean allowOedDownload) {
        this.allowOedDownload = allowOedDownload;
    }

    public String getLinkPreview() {
        return linkPreview;
    }

    public void setLinkPreview(String linkPreview) {
        this.linkPreview = linkPreview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.pages);
        dest.writeByte(this.allowOedDownload ? (byte) 1 : (byte) 0);
        dest.writeString(this.linkPreview);
    }

    protected OedInfo(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.pages = in.readString();
        this.allowOedDownload = in.readByte() != 0;
        this.linkPreview = in.readString();
    }

    public static final Creator<OedInfo> CREATOR = new Creator<OedInfo>() {
        @Override
        public OedInfo createFromParcel(Parcel source) {
            return new OedInfo(source);
        }

        @Override
        public OedInfo[] newArray(int size) {
            return new OedInfo[size];
        }
    };
}




