package br.com.somosdigital.bookshelf.presentation.bookshelf.books;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;

import br.com.solucaoadapta.shared.domain.eventbus.MainBus;
import br.com.solucaoadapta.shared.domain.eventbus.event.UpdateViewEvent;
import br.com.solucaoadapta.shared.domain.log.Log;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadInformation;
import br.com.somosdigital.bookshelf.domain.download.controllers.DownloadStart;
import br.com.somosdigital.bookshelf.domain.download.utils.DownloadStatus;
import br.com.somosdigital.bookshelf.domain.download.utils.RequestCreator;
import br.com.somosdigital.bookshelf.domain.file.FileManager;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Book;
import br.com.somosdigital.bookshelf.domain.model.distribuidor.Oed;
import br.com.somosdigital.bookshelf.storage.repository.book.BookSpecification;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static br.com.converge.download.android.DownloadManager.STATUS_RUNNING;
import static br.com.somosdigital.bookshelf.domain.download.controllers.DownloadController.BOOK_ACTIVITY;
import static br.com.somosdigital.bookshelf.domain.download.controllers.DownloadController.BOOK_COMPLETE;
import static br.com.somosdigital.bookshelf.domain.download.controllers.DownloadController.BOOK_OED;

/**
 * Created by pelisoli on 21/11/16.
 */
public class DownloadActions {

    private BookSpecification bookSpecification;

    private Log log;

    private DownloadStart downloadStart;

    private RequestCreator requestCreator;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public DownloadActions(BookSpecification bookSpecification,
                           DownloadStart downloadStart,
                           RequestCreator requestCreator,
                           Log log) {
        this.bookSpecification = bookSpecification;
        this.downloadStart = downloadStart;
        this.requestCreator = requestCreator;
        this.log = log;
    }

    public DownloadActions(RequestCreator requestCreator) {
        this.requestCreator = requestCreator;
    }

    public Observable<Boolean> pauseDownloadBook(){
        return Observable.defer(() -> {
            boolean end = false;

            DownloadInformation.getDownloadQueue().clear();

            do {

                if(DownloadInformation.getCurrentBookId() != null
                        && !DownloadInformation.getCurrentBookId().isEmpty()
                        && !DownloadInformation.getCurrentBookId().equals("") &&
                        requestCreator.getDownloadManager().query(DownloadInformation.getCurrentDownloadId())
                                == STATUS_RUNNING) {

                    requestCreator.getDownloadManager().pause(DownloadInformation.getCurrentDownloadId());


                } else if (DownloadInformation.getCurrentBookId() != null
                        && DownloadInformation.getCurrentBookId().equals("")) {

                    end = true;

                } else {

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            } while (!end);

            return Observable.just(end);
        });
    }

    public void startDownloadBook(Book book, boolean notifyView, boolean includeOeds) {
        int downloadMode = includeOeds ? BOOK_COMPLETE : BOOK_ACTIVITY;
        book.setDownloadType(downloadMode);

        if (DownloadInformation.getCurrentBookId().equals("")) {
            downloadStart.setBook(book);
            downloadStart.downloadBook();

        } else {

            DownloadInformation.addDownloadQueue(book);
            book.setInQueue(true);

            if (notifyView) {
                MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            }

        }

        log.logInfo("DownloadActions: startDownloadBook");

    }

    public void startDownloadOeds(Book book, boolean notifyView) {

        book.setDownloadType(BOOK_OED);
        if (DownloadInformation.getCurrentBookId().equals("")) {
            downloadStart.setBook(book);
            downloadStart.downloadBook();
        } else {
            DownloadInformation.addDownloadQueue(book);
            book.setInQueue(true);
            if (notifyView) {
                MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            }
        }

        log.logInfo("DownloadActions: startDownloadOeds");

    }

    public void continueDownload(Book book, boolean notifyView) {

        int downloadMode = book.getDownloadType();

        book.setDownloadType(downloadMode);
        if (DownloadInformation.getCurrentBookId().equals("")) {
            downloadStart.setBook(book);
            downloadStart.downloadBook();
        } else {
            DownloadInformation.addDownloadQueue(book);
            book.setInQueue(true);
            if (notifyView) {
                MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
            }
        }

        log.logInfo("DownloadActions: continueDownload");

    }

    public void startDownloadAllBooks(List<Book> bookList, boolean includeOeds) {
        for (Book book : bookList) {
            if (book.getGlobalDownloadStatus() == DownloadStatus.NEW
                    || book.getGlobalDownloadStatus() == DownloadStatus.PAUSED
                    || book.getGlobalDownloadStatus() == DownloadStatus.FAILED
                    || book.getGlobalDownloadStatus() == DownloadStatus.UPDATE) {

                if (!book.isInQueue()) {
                    startDownloadBook(book, false, includeOeds);
                }
            }
        }
    }

    public void pauseDownload(Book book) {
        final Timer timer = new Timer();

        if (book.getGlobalDownloadStatus() != DownloadStatus.PAUSED
                && book.getCardProgress() != null) {
            book.getCardProgress().spinProgress();
        }

        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (book.getGlobalDownloadStatus() != DownloadStatus.PAUSED
                        && book.getGlobalDownloadStatus() != DownloadStatus.SUCCESSFUL
                        && book.getGlobalDownloadStatus() != DownloadStatus.FAILED
                        && book.getGlobalDownloadStatus() != DownloadStatus.CANCELING
                        && book.getGlobalDownloadStatus() != DownloadStatus.NEW) {

                    if (requestCreator.getDownloadManager()
                            .query(DownloadInformation.getCurrentDownloadId()) == STATUS_RUNNING
                            && DownloadInformation.getCurrentBookId().equals(book.getId())) {

                        requestCreator.getDownloadManager().pause(DownloadInformation.getCurrentDownloadId());

                    }
                } else {
                    timer.cancel();
                    timer.purge();
                }

            }
        };

        timer.schedule(task, 0, 1000);
    }

    public void deleteBook(Book book) {
        book.setGlobalDownloadStatus(DownloadStatus.DELETING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        excludeBook(book, false);
    }

    public void deleteBookOeds(Book book) {

        book.setGlobalDownloadStatus(DownloadStatus.DELETING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        excludeBookOeds(book);

    }

    public void updateBook(Book book) {

        book.setGlobalDownloadStatus(DownloadStatus.DELETING);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

        bookUpdate(book);
    }

    public void cancelDownload(Book book) {

        if(book.getCardProgress() != null) {
            book.getCardProgress().spinProgress();
        }

        if (book.getGlobalDownloadStatus() == DownloadStatus.PAUSED
                || book.getGlobalDownloadStatus() == DownloadStatus.FAILED) {

            book.setGlobalDownloadStatus(DownloadStatus.CANCELING);
            MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

            if (book.getDownloadStatus() != DownloadStatus.SUCCESSFUL || (book.getActivity() != null && book.getActivity().getDownloadStatus() != DownloadStatus.SUCCESSFUL)) {
                excludeBook(book, false);
            } else if (book.getDownloadStatus() == DownloadStatus.SUCCESSFUL){
                excludeBookOeds(book);
            }

        } else {
            final Timer timer = new Timer();
            final TimerTask task = new TimerTask() {
                @Override
                public void run() {

                    if (book.getGlobalDownloadStatus() != DownloadStatus.PAUSED
                            && book.getGlobalDownloadStatus() != DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL
                            && book.getGlobalDownloadStatus() != DownloadStatus.ALL_BOOK_SUCCESSFUL
                            && book.getGlobalDownloadStatus() != DownloadStatus.FAILED
                            && book.getGlobalDownloadStatus() != DownloadStatus.CANCELING
                            && book.getGlobalDownloadStatus() != DownloadStatus.NEW) {

                        if (requestCreator.getDownloadManager()
                                .query(DownloadInformation.getCurrentDownloadId()) == STATUS_RUNNING
                                && DownloadInformation.getCurrentBookId().equals(book.getId())) {

                            requestCreator.getDownloadManager().cancel(DownloadInformation.getCurrentDownloadId());
                        }

                    } else {
                        timer.cancel();
                        timer.purge();
                    }

                }
            };

            timer.schedule(task, 0, 1000);

        }

    }

    public void removeBookQueue(Book book) {
        book.setInQueue(false);
        DownloadInformation.getDownloadQueue().remove(book);
        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
    }

    private void excludeBookOeds(Book book) {
        compositeDisposable.add(

                Observable.fromCallable((Callable<Object>) () -> removeOedFiles(book))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                o -> {

                                },

                                throwable -> {

                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("Remove Book: " + throwable.getMessage());
                                    }

                                },

                                () -> {

                                    if (book.getEducationalObjects() != null) {
                                        for (Oed oed : book.getEducationalObjects()) {
                                            oed.setDownloadStatus(DownloadStatus.NEW);
                                        }
                                    }

                                    if (book.getDownloadStatus() == DownloadStatus.UPDATE
                                            || (book.getActivity() != null && book.getActivity().getDownloadStatus() == DownloadStatus.UPDATE)) {
                                        book.setGlobalDownloadStatus(DownloadStatus.UPDATE);
                                    } else {
                                        book.setDownloadStatus(DownloadStatus.SUCCESSFUL);
                                        book.setGlobalDownloadStatus(DownloadStatus.BOOK_ACTIVITY_SUCCESSFUL);
                                    }

                                    book.setCurrentBytesDownloaded(book.getFileLength() + book.getActivity().getFileLength());

                                    bookSpecification.openRealm();
                                    bookSpecification.update(book);
                                    bookSpecification.closeRealm();
                                    MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

                                }
                        )

        );

    }

    private void excludeBook(Book book, boolean removeFromDatabase) {
        compositeDisposable.add(
                Observable.fromCallable((Callable<Object>) () -> excludeWholeBookFiles(book))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                o -> {

                                },

                                throwable -> {

                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("Remove Book: " + throwable.getMessage());
                                    }

                                },

                                () -> {

                                    if (removeFromDatabase) {

                                        bookSpecification.openRealm();
                                        bookSpecification.remove(book);
                                        bookSpecification.closeRealm();

                                    } else {

                                        book.setDownloadStatus(DownloadStatus.NEW);

                                        if (book.getActivity() != null) {
                                            book.getActivity().setDownloadStatus(DownloadStatus.NEW);
                                        }

                                        if (book.getEducationalObjects() != null) {
                                            for (Oed oed : book.getEducationalObjects()) {
                                                oed.setDownloadStatus(DownloadStatus.NEW);
                                            }
                                        }

                                        book.setGlobalDownloadStatus(DownloadStatus.NEW);
                                        book.setCurrentBytesDownloaded(0);

                                        bookSpecification.openRealm();
                                        bookSpecification.update(book);
                                        bookSpecification.closeRealm();
                                        MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));
                                    }

                                }
                        )
        );

    }

    private void bookUpdate(Book book) {
        compositeDisposable.add(
                Observable.fromCallable((Callable<Object>) () -> (true))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                o -> {

                                },

                                throwable -> {

                                    if(throwable != null && throwable.getMessage() != null) {
                                        log.logError("Remove Book: " + throwable.getMessage());
                                    }

                                },

                                () -> {

                                    if (book.getDownloadStatus() == DownloadStatus.UPDATE) {
                                        book.setCurrentBytesDownloaded(0);
                                        removeBookFile(book);
                                    }

                                    if (book.getActivity() != null) {
                                        if (book.getActivity().getDownloadStatus() == DownloadStatus.UPDATE) {
                                            book.getActivity().setDownloadStatus(DownloadStatus.NEW);
                                        }
                                    }

                                    bookSpecification.openRealm();
                                    bookSpecification.update(book);
                                    bookSpecification.closeRealm();
                                    MainBus.getInstance().post(new UpdateViewEvent(book.getBookPosition()));

                                    startDownloadBook(book, true, false);

                                }
                        )
        );
    }

    private boolean excludeWholeBookFiles(Book book) {
        FileManager.deleteFile(book.getCompactedPath());
        FileManager.removeDirectory(book.getTempPath());
        FileManager.deleteFile(book.getBrkPath());
        FileManager.removeDirectory(book.getBookPath());
        return true;
    }

    private void removeBookFile(Book book){
        FileManager.deleteFile(book.getCompactedPath());
        FileManager.deleteFile(book.getBrkPath());
    }

    private boolean removeOedFiles(Book book) {

        if (book.getEducationalObjects() != null){
            for (Oed oed : book.getEducationalObjects()){
                if (oed.getCompactedPath() != null) {
                    FileManager.deleteFile(oed.getCompactedPath());
                }
                if (oed.getTempPath() != null) {
                    FileManager.removeDirectory(oed.getTempPath());
                }
                if (oed.getBrkPath() != null) {
                    FileManager.deleteFile(oed.getBrkPath());
                }
                if (oed.getOedPath() != null){
                    FileManager.removeDirectory(oed.getOedPath());
                }
            }
        }

        return true;

    }

}
