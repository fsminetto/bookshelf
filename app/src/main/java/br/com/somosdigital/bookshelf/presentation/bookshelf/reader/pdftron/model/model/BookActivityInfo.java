package br.com.somosdigital.bookshelf.presentation.bookshelf.reader.pdftron.model.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jgsmanaroulas on 17/01/17.
 */
public class BookActivityInfo implements Parcelable {

    private String Id;
    private String Name;
    private String Type;
    private String Page;
    private boolean isSelected;

    protected BookActivityInfo(Parcel in) {
        Id = in.readString();
        Name = in.readString();
        Type = in.readString();
        Page = in.readString();
        isSelected = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Id);
        dest.writeString(Name);
        dest.writeString(Type);
        dest.writeString(Page);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookActivityInfo> CREATOR = new Creator<BookActivityInfo>() {
        @Override
        public BookActivityInfo createFromParcel(Parcel in) {
            return new BookActivityInfo(in);
        }

        @Override
        public BookActivityInfo[] newArray(int size) {
            return new BookActivityInfo[size];
        }
    };

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getPage() {
        return Page;
    }

    public void setPage(String page) {
        Page = page;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
