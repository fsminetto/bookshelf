package br.com.somosdigital.bookshelf.domain.activity;

/**
 * Created by pelisoli on 06/12/16.
 */

public interface AdaptaActivity {

    String getLocalPath(boolean isTeacher, boolean canSendActivty);

    String getWebPath(boolean isTeacher, boolean canSendActivty);

    String getLocalTemplate(boolean isTeacher, boolean canSendActivty);

    boolean templateExists();

    boolean activityExists();

    ActivityInfo getActivityInfo();

    String getActivityJsonPath();

    String getLocalActivityPath();

    String getActivityResourcesPath();
}
