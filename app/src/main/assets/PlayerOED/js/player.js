window.onload = function() {
    //instancia a classe necessária para a abertura do OED.
    var oed = new oedPlayer();
    
    //Inicia o carregamento do OED a partir da URL recuperada.
    oed.loadParams();
};


/**
 * Classe responsável por decriptografar e carregar o OED no iframe
 */
function oedPlayer () {
    
    
    this.loadParams = function() {
        
        var path = this.getParameterByName("pathOed").toString();
        var key = this.getParameterByName("oedKey").toString();
        
        //Inicia o carregamento do OED
        this.openOED(path, key.toString().substring(0,16) /*tratamento para pegar apenas os 16 primeiros caracteres da chave de criptografia*/);
    };
    
    
    /**
     * Método que realiza a abertura do index.html no iframe
     * path (string): path do oed
     * key (string): chave de descriptografia
     */
    this.openOED = function(path, key){
        
        //Referência do iframe
        var iframe = document.getElementById('iframeContent');
        
        //Realiza o carregamento do index.html
        var req = new XMLHttpRequest();
        req.open("GET", path, true);
        
        //Referencia da função de remoção de links com ancoras
        var removeHrefHashtag = this.removeHrefHashtag;
                                                
        req.onload = function (e) {
            
            var data = req.response;
            
            //Verifica se os dados foram recuperados
            if (data) {
                
                //Variável que guardará o conteúdo do index.html
                var indexHtmlContent;
                
                //Verifica se não foi passada nenhuma chave de criptografia
                if (key === "") {
                    //Caso não tenha chave, significa que o arquivo não possui criptografia
                    indexHtmlContent = data;
                } else {
                    //Realiza a descriptografia do conteúdo
                    key = CryptoJS.enc.Hex.parse(pidCryptUtil.convertToHex(key));
                    indexHtmlContent = CryptoJS.AES.decrypt(data, key, {iv: CryptoJS.enc.Hex.parse('00000000000000000000000000000000')}).toString(CryptoJS.enc.Utf8);
                }
                
                //Caminho da pasta do OED onde o index.html está
                var indexHtmlFolderPath = path.split("/");
                indexHtmlFolderPath.pop()
                indexHtmlFolderPath = indexHtmlFolderPath.join("/");
                
                //Insere a TAG Base no html do conteúdo do index.html para que referencie sua pasta de origem, não a pasta do playerOED
                var base = "<base href=\"" + indexHtmlFolderPath + "/" + "\"/>";
                indexHtmlContent = indexHtmlContent.replace(/(<head.*?>)/, "$1" + base);
                
                //Após o carregamento do OED, realiza os ajustes de redimensionamento
                iframe.onload = function () {
                    iframe.style.position = "absolute";
                    iframe.style.height   = "100%";
                    iframe.style.width    = "100%";
                    iframe.style.margin   = "0";
                    iframe.style.padding  = "0";
                    iframe.style.border   = "0";
                    
                    //Adiciona os eventos para capturar toque na tela e remover os links com ancoras.
                    iframe.contentWindow.document.body.addEventListener('touchstart', removeHrefHashtag, false);
                    iframe.contentWindow.document.body.addEventListener('mousedown', removeHrefHashtag, false);
                };
                
                //Adiciona o conteúdo carregado e com os tratamentos no iframe.
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.write(indexHtmlContent);
                iframe.contentWindow.document.close();
                
            }
        };
        
        req.send(null);
    };
    
    /**
     * Realiza a busca dentro da tag BODY do iframe removendo os links com ancoras.
     * event: Evento de touch ou mousedown
     */
    this.removeHrefHashtag = function (event) {

        var anchors = document.getElementById('iframeContent').contentWindow.document.body.getElementsByTagName('a');

        for (var i = 0; i < anchors.length; i++) {
            if (anchors[i].href.indexOf("/#") > -1) {
                anchors[i].removeAttribute("href");
            }
        }
    };
    
    
    /**
     * Retorna o parametro da URL solicitado pelo nome
     * parameterName (string): Nome do parametro
     */
    this.getParameterByName = function (parameterName) {
        parameterName = parameterName.replace("/[\[]/", "\\[").replace("/[\]]/", "\\]");
        var regex = new RegExp("[\\?&]" + parameterName + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
};